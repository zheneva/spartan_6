library IEEE;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity cntrlLed_test is
end cntrlLed_test;

architecture cntrlLed_test_struct of cntrlLed_test is

component cntrlLed is
    port (
      clk : in std_logic;
      rst : in std_logic;
      btn : in std_logic;
      leds_o : out std_logic_vector(11 downto 0) 
    ) ;
end component;    

    signal clk : std_logic := '0'; 
    signal rst : std_logic ; 
    signal btn : std_logic := '1'; 
    signal leds_o : std_logic_vector(11 downto 0);

begin

-- rst <= '1' after 1790 ns,
--        '0' after 1800 ns;

process
 begin
   wait for 10 ns;
   clk <= not clk; 
end process;

process
 begin
    wait for 1000 ns;
    wait for 45 ns;
    btn <= '0';
    wait for 155 ns;
    btn <= '1';
    wait for 390 ns;
    btn <= '0';
    wait for 20 ns;
    btn <= '1';
end process;    


circ: cntrlLed
        port map(
            clk => clk,
            rst => rst,
            btn => btn,
            leds_o => leds_o 
        ) ;

end cntrlLed_test_struct ; -- arch