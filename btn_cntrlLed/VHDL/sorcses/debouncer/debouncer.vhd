library IEEE;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;


entity debouncer is
  generic(
    DELAY : integer
  );
  port (
    clk : in std_logic;
    i_data : in std_logic;
    o_data : out std_logic
  ) ;
end debouncer;


architecture debouncer_struct of debouncer is

    signal delay_cnt : std_logic_vector(15 downto 0) ;
    signal SCLR : std_logic;
    signal FF_enable : std_logic;

    signal ff_1_out : std_logic;
    signal ff_2_out : std_logic;

begin


FF_1_2 : process( clk )
 begin
  if rising_edge(clk) then
    ff_1_out <= i_data;
    ff_2_out <= ff_1_out;
  end if ;
end process ; -- debouncer

SCLR <= ff_1_out XOR ff_2_out;

control_cnt : process( clk )
begin
  if rising_edge(clk) then

    if SCLR = '0' then
      if unsigned(delay_cnt) = DELAY then
        delay_cnt <= delay_cnt; --!(others => '0');
        FF_enable <= '1';
      else
        delay_cnt <= std_logic_vector(unsigned(delay_cnt) + 1) ;
      end if ;
    else
      delay_cnt <= (others => '0');
      FF_enable <= '0';
    end if ;
    
  end if ;
end process ; -- control_cnt



FF_3 : process( clk )
begin
  if rising_edge(clk) then
    if FF_enable = '1' then
      o_data <= ff_2_out;
    end if ;
  end if ;
end process ; -- FF_3
    

end debouncer_struct ; -- debouncer_struct