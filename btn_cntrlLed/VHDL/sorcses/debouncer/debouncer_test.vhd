library IEEE;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity debouncer_test is
end debouncer_test;


architecture debouncer_test_struct of debouncer_test is

component debouncer
    generic(
      DELAY : integer
    );
    port (
      clk : in std_logic;
      i_data : in std_logic := '1';
      o_data : out std_logic
    ) ;
end component;


    signal clk    : std_logic := '0';
    signal i_data : std_logic := '1';
    signal o_data : std_logic;

begin
  
process
 begin
  wait for 10 ns;
  clk <= not clk; 
end process;

i_data <= '0' after 3 ns,
          '1' after 8 ns,
          '0' after 15 ns,
          '1' after 25 ns,
          '0' after 30 ns,
          '1' after 370 ns,
          '0' after 375 ns,
          '1' after 378 ns,
          '0' after 383 ns,
          '1' after 390 ns;


circ_deb: debouncer
        generic map(
            DELAY => 10
        )
        port map(
            clk    => clk    ,
            i_data => i_data ,
            o_data => o_data 
        ) ;

end debouncer_test_struct ; -- debouncer_test_struct