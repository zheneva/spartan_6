library IEEE;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity cntrlLed is
  port (
    clk : in std_logic;
    rst : in std_logic;
    btn : in std_logic;
    no_bip : out std_logic; 
    leds_o : out std_logic_vector(11 downto 0) 
  ) ;
end cntrlLed;


architecture cntrlLed_struct of cntrlLed is

component debouncer
    generic(
        DELAY : integer
    );
    port (
        clk : in std_logic;
        i_data : in std_logic;
        o_data : out std_logic
    ) ;
end component;


--!-----------------------------------------------
    -- type controller_state is (NO_LIGHT, NO_LIGHT_PRESS, LIGHT, LIGHT_PRESS);
    -- signal State, nextState : controller_state;

    -- signal state_cnt : std_logic_vector(15 downto 0) := (others => '0');
--!--------------------------------------------------

    type controller_state is (RESET, HL_rs, HL_1, HL_2, HL_3, HL_4, HL_5, HL_6, HL_7, HL_8, HL_9, HL_10, HL_11, HL_12,  
                                            Bl_1, Bl_2, Bl_3, Bl_4, Bl_5, Bl_6, Bl_7, Bl_8, Bl_9, Bl_10, Bl_11, Bl_12  
    );

    signal debounsed_btn : std_logic;

    signal State, nextState : controller_state := RESET;

    signal control_cnt : std_logic_vector( 3 downto 0) := "1100" ;

begin




FSM_STATE : process( clk )
 begin
    if rising_edge(clk) then
        if rst = '0' then 
            State <= RESET;
        else
            State <= nextState;
        end if; 
    end if;
end process ; -- FSM_STATE


state_logic : process( debounsed_btn, State )
 begin

    case( State ) is

        when RESET => 
            if ( debounsed_btn = '1' ) then
                nextState <= RESET;
            else
                nextState <= HL_rs;
            end if ;
    
        when HL_rs => 
                if debounsed_btn = '1' then
                    nextState <= Bl_1;
                else
                    nextState <= HL_rs;
                end if ;

        when Bl_1 => 
            if ( debounsed_btn = '1' ) then
                nextState <= Bl_1;
            else
                nextState <= Hl_1;
            end if ;
    
        when HL_1 => 
                if debounsed_btn = '1' then
                    nextState <= Bl_2;
                else
                    nextState <= HL_1;
                end if ;
            

        when Bl_2 => 
                if ( debounsed_btn = '1' ) then
                    nextState <= Bl_2;
                else
                    nextState <= Hl_2;
                end if ;
        
        when HL_2 => 
                    if debounsed_btn = '1' then
                        nextState <= Bl_3;
                    else
                        nextState <= HL_2;
                    end if ;

        when Bl_3 => 
                if ( debounsed_btn = '1' ) then
                    nextState <= Bl_3;
                else
                    nextState <= Hl_3;
                end if ;
        
        when HL_3 => 
                    if debounsed_btn = '1' then
                        nextState <= Bl_4;
                    else
                        nextState <= HL_3;
                    end if ;

        when Bl_4 => 
                if ( debounsed_btn = '1' ) then
                    nextState <= Bl_4;
                else
                    nextState <= Hl_4;
                end if ;
        
        when HL_4 => 
                if debounsed_btn = '1' then
                    nextState <= Bl_5;
                else
                    nextState <= HL_4;
                end if ;

        when Bl_5 => 
                if ( debounsed_btn = '1' ) then
                    nextState <= Bl_5;
                else
                    nextState <= Hl_5;
                end if ;

        when HL_5 => 
                if debounsed_btn = '1' then
                    nextState <= Bl_6;
                else
                    nextState <= HL_5;
                end if ;

        when Bl_6 => 
                if ( debounsed_btn = '1' ) then
                    nextState <= Bl_6;
                else
                    nextState <= Hl_6;
                end if ;
        
        when HL_6 => 
                if debounsed_btn = '1' then
                    nextState <= Bl_7;
                else
                    nextState <= HL_6;
                end if ;

        when Bl_7 => 
                if ( debounsed_btn = '1' ) then
                    nextState <= Bl_7;
                else
                    nextState <= Hl_7;
                end if ;
            
        when HL_7 => 
                if debounsed_btn = '1' then
                    nextState <= Bl_8;
                else
                    nextState <= HL_7;
                end if ;

        when Bl_8 => 
                if ( debounsed_btn = '1' ) then
                    nextState <= Bl_8;
                else
                    nextState <= Hl_8;
                end if ;
        
        when HL_8 => 
                if debounsed_btn = '1' then
                    nextState <= Bl_9;
                else
                    nextState <= HL_8;
                end if ;

        when Bl_9 => 
                if ( debounsed_btn = '1' ) then
                    nextState <= Bl_9;
                else
                    nextState <= Hl_9;
                end if ;
        
        when HL_9 => 
                if debounsed_btn = '1' then
                    nextState <= Bl_10;
                else
                    nextState <= HL_9;
                end if ;

        when Bl_10 => 
                if ( debounsed_btn = '1' ) then
                    nextState <= Bl_10;
                else
                    nextState <= Hl_10;
                end if ;
        
        when HL_10 => 
                if debounsed_btn = '1' then
                    nextState <= Bl_11;
                else
                    nextState <= HL_10;
                end if ;

        when Bl_11 => 
                if ( debounsed_btn = '1' ) then
                    nextState <= Bl_11;
                else
                    nextState <= Hl_11;
                end if ;
        
        when HL_11 => 
                if debounsed_btn = '1' then
                    nextState <= Bl_12;
                else
                    nextState <= HL_11;
                end if ; 
                
        when Bl_12 => 
                if ( debounsed_btn = '1' ) then
                    nextState <= Bl_12;
                else
                    nextState <= Hl_12;
                end if ;
        
        when HL_12 => 
                if debounsed_btn = '1' then
                    nextState <= RESET;
                else
                    nextState <= HL_12;
                end if ;

        when others => nextState <= RESET;
    
    end case ;
    
end process ; -- state_logic

leds_o <=   "111111111111" when State = RESET  else 
            "111111111110" when State = BL_1  else
            "111111111100" when State = BL_2  else
            "111111111000" when State = BL_3  else
            "111111110000" when State = BL_4  else
            "111111100000" when State = BL_5  else
            "111111000000" when State = BL_6  else
            "111110000000" when State = BL_7  else
            "111100000000" when State = BL_8  else
            "111000000000" when State = BL_9  else
            "110000000000" when State = BL_10 else
            "100000000000" when State = BL_11 else
            "000000000000" when State = BL_12 else
            "111111111111" 
;






debouncer_circ:  debouncer
    generic map(
        DELAY => 20000        --? while 50 Mhz clk => DELAY = 0,4 ms
    )
    port map(
        clk => clk,
        i_data => btn,
        o_data => debounsed_btn
) ;        

-- out_logic : process( State )

-- end process ; -- out_logic

--!-------------------------------------------\

-- debouncer_circ:  debouncer
--         generic map(
--             DELAY => 1000
--         )
--         port map(
--             clk => clk,
--             i_data => btn,
--             o_data => debounsed_btn
--         ) ;


-- FSM_STATE : process( clk )
--  begin
--     if rising_edge(clk) then
--         if rst = '0' then 
--             State <= NO_LIGHT;
--         else
--             State <= nextState;
--         end if; 
--     end if;
-- end process ; -- FSM_STATE


-- state_logic : process( btn, State)
-- begin

--     case( State ) is
    
--         when NO_LIGHT =>
--             if ( debounsed_btn = '1' ) then 
--                 nextState <= NO_LIGHT;
--             else
--                 nextState <= NO_LIGHT_PRESS ;
--             end if;
        
--         when NO_LIGHT_PRESS => 
--             if ( debounsed_btn = '0') then 
--                 nextState <= NO_LIGHT_PRESS;
--                 --state_cnt <= std_logic_vector(unsigned(state_cnt) + 1) ;
--             else    
--                 nextState <= LIGHT;
--             end if;

--         when LIGHT => 
--             if debounsed_btn = '1' then 
--                 nextState <= LIGHT;
--             else    
--                 nextState <= LIGHT_PRESS;
--             end if;

--         when LIGHT_PRESS => 
--             if debounsed_btn = '0' then 
--                 nextState <= LIGHT_PRESS;
--             else    
--                 nextState <= NO_LIGHT;
--             end if;

--         when others => nextState <= NO_LIGHT;
    
--     end case ;
    
-- end process ; -- state_logic


-- leds_o <= (others => '0') when State = LIGHT_PRESS or State = LIGHT  else (others => '1');



-- process(clk)
--  begin 
--  if btn = '1'  then
--     leds_o <= (others => '0');
--     --leds_o( 5 downto 0) <= (others => '1');
--  else
--     leds_o <= (others => '1');
-- --     leds_o( 5 downto 0) <= (others => '0');
--  end if ;
-- end process;
no_bip <= '1';

end cntrlLed_struct ; -- cntrlLed