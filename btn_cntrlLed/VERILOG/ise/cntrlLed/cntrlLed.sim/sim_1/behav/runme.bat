@echo off


rem  PlanAhead(TM)
rem  runme.bat: a PlanAhead-generated ISim simulation Script
rem  Copyright 1986-1999, 2001-2013 Xilinx, Inc. All Rights Reserved.


set PATH=%XILINX%\lib\%PLATFORM%;%XILINX%\bin\%PLATFORM%;D:/xilinx/14.7/ISE_DS/EDK/bin/nt64;D:/xilinx/14.7/ISE_DS/EDK/lib/nt64;D:/xilinx/14.7/ISE_DS/ISE/bin/nt64;D:/xilinx/14.7/ISE_DS/ISE/lib/nt64;D:/xilinx/14.7/ISE_DS/common/bin/nt64;D:/xilinx/14.7/ISE_DS/common/lib/nt64;D:/xilinx/14.7/ISE_DS/PlanAhead/bin;%PATH%

set XILINX_PLANAHEAD=D:/xilinx/14.7/ISE_DS/PlanAhead

fuse -intstyle pa -incremental -L work -L unisims_ver -L unimacro_ver -L xilinxcorelib_ver -L secureip -o cntrlLed_test.exe --prj D:/Androsov/spartan_6/btn_cntrlLed/VERILOG/ise/cntrlLed/cntrlLed.sim/sim_1/behav/cntrlLed_test.prj -top work.cntrlLed_test -top work.glbl
if errorlevel 1 (
   cmd /c exit /b %errorlevel%
)
