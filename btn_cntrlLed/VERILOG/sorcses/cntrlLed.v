

module cntrlLed (
    input clk,
    input rst,
    input btn,
    output reg no_bip,
    output reg [11:0] leds_o
);
    
    localparam [1:0] RESET = 2'b00, HOLD = 2'b01, INC = 2'b10, LIGHT = 2'b11 ;

    reg [1:0] State, nextState;

    reg [11:0] control_cnt = 'd11;
    
    wire debounce_btn;

    always @(posedge clk) begin
        if (rst == 0) begin
            State <= RESET;
        end else begin 
            State <= nextState;
        end
    end

    always @(posedge clk) begin
        if (State == RESET) begin
            control_cnt <= 'd12;
        end else if (State == INC ) begin
                if (control_cnt == 12) begin
                    control_cnt <= 'd0;
                end else begin
                    control_cnt <= control_cnt + 1;
                end
            end
    end

    always @(*) begin

        case (State)
            RESET : begin
                    if (debounce_btn == 1) 
                        nextState <= RESET;
                    else    
                        nextState <= HOLD;
            end

            HOLD  : begin
                    if(debounce_btn == 0) 
                        nextState <= HOLD;
                    else
                        nextState <= INC;
            end

            INC   : begin
                    nextState <= LIGHT; 
            end

            LIGHT : begin
                    if (debounce_btn == 1) 
                        nextState <= LIGHT;
                    else if (control_cnt == 12) 
                            nextState <= RESET;
                        else
                            nextState <= HOLD;
            end

            default: 
                    nextState <= RESET;
        endcase
    end

always @(control_cnt) begin
    case (control_cnt)
        'd0    : leds_o <= 12'b111111111110;
        'd1    : leds_o <= 12'b111111111100; 
        'd2    : leds_o <= 12'b111111111000;
        'd3    : leds_o <= 12'b111111110000;
        'd4    : leds_o <= 12'b111111100000;
        'd5    : leds_o <= 12'b111111000000;
        'd6    : leds_o <= 12'b111110000000;
        'd7    : leds_o <= 12'b111100000000;
        'd8    : leds_o <= 12'b111000000000;
        'd9    : leds_o <= 12'b110000000000;
        'd10   : leds_o <= 12'b100000000000;
        'd11   : leds_o <= 12'b000000000000;
        'd12   : leds_o <= 12'b111111111111;
        default: leds_o <= 12'b111111111111;
    endcase
end

always @(posedge clk) begin
    no_bip <= 1'b1;
end

debouncer #( .DELAY(250000)) debounce_circ
(
        .clk(clk)
    ,   .i_data(btn)
    ,   .o_data(debounce_btn)
);

endmodule