

module debouncer 
#(parameter DELAY = 25000)
(
    input  clk,
    input  i_data,
    output reg o_data
);

reg ff_1_out, ff_2_out, ff_3_out; 
reg FF_enable;
reg [25:0] delay_cnt = 'd0;
wire sclr;  

always @(posedge clk) begin
    ff_1_out <= i_data;
    ff_2_out <= ff_1_out;
end

assign sclr = (ff_1_out & ff_2_out);


always @(posedge clk) begin
    if (sclr == 0) begin
        if (delay_cnt == DELAY) begin
            delay_cnt <= delay_cnt;
            FF_enable <= 1'b1;
        end else begin
            delay_cnt <= delay_cnt + 'd1;
        end
    end else begin
        delay_cnt <= 'd0;
        FF_enable <= 1'b0;
    end    
end

always @(posedge clk) begin
    if (FF_enable == 1) begin
        o_data <= ff_2_out;
    end
end

endmodule