
module cntrlLed_test ();


reg clk;
reg rst;
reg btn;
wire no_bip;
wire [11:0] leds_o;

initial begin
    rst <= 1'b0; btn <= 1'b1;

forever begin
    #100 btn = 1'b0; 
    #30  btn = 1'b1; 
    #50  btn = 1'b0; 
    #30  btn = 1'b1;
    #100 btn = 1'b0; 
    #170  btn = 1'b1; 
    #50  btn = 1'b0; 
    #230  btn = 1'b1;
end

end

initial clk = 0;
always #10 clk = ~clk;






cntrlLed circ_1(
        .clk(clk)
    ,   .rst(rst)
    ,   .btn(btn)
    ,   .no_bip(no_bip)
    ,   .leds_o(leds_o)
);

endmodule