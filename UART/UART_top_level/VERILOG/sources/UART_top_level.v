


module UART_top_level (
  input clk,
  input i_RX_serial,
  output o_TX_serial,
  output reg no_bip
);
  
wire [7:0] s_RX_byte;
wire       s_RX_DV;

uart_receiver #(  
       .CLKs_per_BIT(434)
    ,  .serial_bits_number(8)
) UR_receiver
(
       .clk(clk)
    ,  .i_RX_serial(i_RX_serial)
    ,  .o_RX_byte(s_RX_byte)
    ,  .o_RX_DV(s_RX_DV)
);

uart_transmitter #(
       .CLKs_per_BIT(434)
    ,  .serial_bits_number(8)
) UR_transmit
(
       .clk(clk)
    ,  .i_TX_DV(s_RX_DV)
    ,  .i_TX_byte(s_RX_byte)
    ,  .o_TX_serial(o_TX_serial)
);


always @(posedge clk) begin
  no_bip <= 1'b1;
end

endmodule