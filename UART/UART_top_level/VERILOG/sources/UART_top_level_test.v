

module UART_top_level_test (
);
    

reg [10:0] cnt;
reg clk;
reg color_RX_serial;                          //! helps ricognise the start/data/stop areas of the input data
reg i_RX_serial;
wire o_TX_serial;
integer i;


initial begin
    clk = 1'b0;
    i_RX_serial = 1'b1;
    color_RX_serial = 1'b1;
    #10;

forever begin                                         //!                      *217 due to 25MHz clk and 115200 data rate => new data bit appears every 217 clk cycle 
    #(240*217)                                        //! frame break delay   **comment to disable frame break

    i_RX_serial = 1'b0;                          //? start bit (z)
    color_RX_serial  = 1'bz;                     //? start bit (z)
    
    for (i = 0; i<8; i=i+1) begin
        // #(40*217) i_RX_serial = 1'bx;
        // #(40*217) color_RX_serial = 1'bx;
        #(40*217) 
                    //counter_RX_serial = counter_RX_serial+1;
                    i_RX_serial = $random;
                    //i_RX_serial = ~i_RX_serial;
                    color_RX_serial = 1;
    end

    #(40*217) i_RX_serial = 1'b1;                      //? stop bit (z)
        color_RX_serial = 1'bz;                        //? stop bit (z)
    #(40*217) i_RX_serial = 1'b1;                      //! frame break (x)
        color_RX_serial = 1'bx;                        //! frame break (x)

end

end

always  #20 clk = ~clk;



UART_top_level UR_top_level(
    .clk(clk)
  , .i_RX_serial(i_RX_serial)
  , .o_TX_serial(o_TX_serial)
);

endmodule