
# PlanAhead Launch Script for Pre-Synthesis Floorplanning, created by Project Navigator

create_project -name uart_top_level_prjct -dir "D:/Androsov/spartan_6/UART/UART_top_level/VERILOG/ise/uart_top_level_prjct/planAhead_run_3" -part xc6slx9tqg144-3
set_param project.pinAheadLayout yes
set srcset [get_property srcset [current_run -impl]]
set_property target_constrs_file "UART_top_level.ucf" [current_fileset -constrset]
set hdlfile [add_files [list {../../../../UART_transmitter/VERILOG/sources/uart_transmitter.v}]]
set_property file_type Verilog $hdlfile
set_property library work $hdlfile
set hdlfile [add_files [list {../../../../UART_receiver/VERILOG/sources/uart_receiver.v}]]
set_property file_type Verilog $hdlfile
set_property library work $hdlfile
set hdlfile [add_files [list {../../sources/UART_top_level.v}]]
set_property file_type Verilog $hdlfile
set_property library work $hdlfile
set_property top UART_top_level $srcset
add_files [list {UART_top_level.ucf}] -fileset [get_property constrset [current_run]]
open_rtl_design -part xc6slx9tqg144-3
