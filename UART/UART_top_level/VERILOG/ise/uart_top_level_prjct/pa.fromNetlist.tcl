
# PlanAhead Launch Script for Post-Synthesis pin planning, created by Project Navigator

create_project -name uart_top_level_prjct -dir "D:/Androsov/spartan_6/UART/UART_top_level/VERILOG/ise/uart_top_level_prjct/planAhead_run_2" -part xc6slx9tqg144-3
set_property design_mode GateLvl [get_property srcset [current_run -impl]]
set_property edif_top_file "D:/Androsov/spartan_6/UART/UART_top_level/VERILOG/ise/uart_top_level_prjct/UART_top_level.ngc" [ get_property srcset [ current_run ] ]
add_files -norecurse { {D:/Androsov/spartan_6/UART/UART_top_level/VERILOG/ise/uart_top_level_prjct} }
set_param project.pinAheadLayout  yes
set_property target_constrs_file "UART_top_level.ucf" [current_fileset -constrset]
add_files [list {UART_top_level.ucf}] -fileset [get_property constrset [current_run]]
link_design
