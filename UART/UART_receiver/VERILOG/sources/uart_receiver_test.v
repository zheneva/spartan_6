// CLKS_PER_BIT = (Frequency of i_Clock)/(Frequency of UART)
// Example: 25 MHz Clock, 115200 baud UART
// (25000000)/(115200) = 217


// 40 ns clk period => 25MHz

module uart_receiver_test #(
    parameter CLKs_per_BIT = 434
)
(
);
    
reg [10:0] cnt;
reg clk;
reg color_RX_serial;                          //! helps ricognise the start/data/stop areas of the input data
reg i_RX_serial;
wire [7:0] o_RX_byte;
wire o_RX_DV;
integer i;


initial begin
    clk = 1'b0;
    i_RX_serial = 1'b1;
    color_RX_serial = 1'b1;
    #(100*CLKs_per_BIT);

// forever begin                                         //!                      *217 due to 25MHz clk and 115200 data rate => new data bit appears every 217 clk cycle 
//     //!#(100*CLKs_per_BIT)                                        //! frame break delay   **comment to disable frame break

//     i_RX_serial = 1'b0;                          //? start bit (z)
//     color_RX_serial  = 1'bz;                     //? start bit (z)
    
//     for (i = 0; i<8; i=i+1) begin
//         // #(40*217) i_RX_serial = 1'bx;
//         // #(40*217) color_RX_serial = 1'bx;
//         //!#(20*217)
//         #(20*CLKs_per_BIT)

//                     //counter_RX_serial = counter_RX_serial+1;
//                     //i_RX_serial = $random;
//                     i_RX_serial = ~i_RX_serial;
//                     color_RX_serial = 1;
//     end

//     //!#(20*217)
//     #(20*CLKs_per_BIT)
//      i_RX_serial = 1'b1;                      //? stop bit (z)
//         color_RX_serial = 1'bz;                        //? stop bit (z)
//     //!#(20*217)
//     #(20*CLKs_per_BIT)
//      i_RX_serial = 1'b1;                      //! frame break (x)
//         color_RX_serial = 1'bx;                        //! frame break (x)

// end


forever begin

    //!i_RX_serial = 1;     #(20*CLKs_per_BIT);
    i_RX_serial = 0;     #(20*CLKs_per_BIT);
    i_RX_serial = 0;     #(20*CLKs_per_BIT);
    i_RX_serial = 0;     #(20*CLKs_per_BIT);
    i_RX_serial = 0;     #(20*CLKs_per_BIT);
    i_RX_serial = 0;     #(20*CLKs_per_BIT);
    i_RX_serial = 0;     #(20*CLKs_per_BIT);
    i_RX_serial = 0;     #(20*CLKs_per_BIT);
    i_RX_serial = 0;     #(20*CLKs_per_BIT);
    i_RX_serial = 0;     #(20*CLKs_per_BIT);
    i_RX_serial = 1;     #(20*CLKs_per_BIT);


    i_RX_serial = 0;     #(20*CLKs_per_BIT);
    i_RX_serial = 0;     #(20*CLKs_per_BIT);
    i_RX_serial = 0;     #(20*CLKs_per_BIT);
    i_RX_serial = 0;     #(20*CLKs_per_BIT);
    i_RX_serial = 0;     #(20*CLKs_per_BIT);
    i_RX_serial = 0;     #(20*CLKs_per_BIT);
    i_RX_serial = 0;     #(20*CLKs_per_BIT);
    i_RX_serial = 1;     #(20*CLKs_per_BIT);
    i_RX_serial = 1;     #(20*CLKs_per_BIT);
    i_RX_serial = 1;     #(20*CLKs_per_BIT);

    i_RX_serial = 0;     #(20*CLKs_per_BIT);
    i_RX_serial = 0;     #(20*CLKs_per_BIT);
    i_RX_serial = 0;     #(20*CLKs_per_BIT);
    i_RX_serial = 0;     #(20*CLKs_per_BIT);
    i_RX_serial = 0;     #(20*CLKs_per_BIT);
    i_RX_serial = 0;     #(20*CLKs_per_BIT);
    i_RX_serial = 1;     #(20*CLKs_per_BIT);
    i_RX_serial = 1;     #(20*CLKs_per_BIT);
    i_RX_serial = 1;     #(20*CLKs_per_BIT);
    i_RX_serial = 1;     #(20*CLKs_per_BIT);

    i_RX_serial = 0;     #(20*CLKs_per_BIT);
    i_RX_serial = 0;     #(20*CLKs_per_BIT);
    i_RX_serial = 0;     #(20*CLKs_per_BIT);
    i_RX_serial = 0;     #(20*CLKs_per_BIT);
    i_RX_serial = 0;     #(20*CLKs_per_BIT);
    i_RX_serial = 1;     #(20*CLKs_per_BIT);
    i_RX_serial = 1;     #(20*CLKs_per_BIT);
    i_RX_serial = 1;     #(20*CLKs_per_BIT);
    i_RX_serial = 1;     #(20*CLKs_per_BIT);
    i_RX_serial = 0;     #(20*CLKs_per_BIT);

    i_RX_serial = 1;     #(20*CLKs_per_BIT);

end

end

always  #10 clk = ~clk;



uart_receiver #( .CLKs_per_BIT(CLKs_per_BIT),  .serial_bits_number(8) ) UR
(
        .clk(clk)
    ,   .i_RX_serial(i_RX_serial)
    ,   .o_RX_byte(o_RX_byte)
    ,   .o_RX_DV(o_RX_DV)
);
endmodule