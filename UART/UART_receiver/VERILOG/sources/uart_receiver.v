// CLKS_PER_BIT = (Frequency of i_Clock)/(Frequency of UART)
// Example: 25 MHz Clock, 115200 baud UART
// (25000000)/(115200) = 217

// Example: 50 MHz Clock, 115200 baud UART
// (25000000)/(115200) = 434

module uart_receiver 
#(
    parameter  CLKs_per_BIT = 10,
    parameter  serial_bits_number = 8
)
(
    input  clk,
    input  i_RX_serial,
    output reg [serial_bits_number-1:0] o_RX_byte,
    output reg o_RX_DV
);
    

    localparam [2:0] RST  = 3'b000;
    localparam [2:0] IDLE = 3'b001;
    localparam [2:0] RX   = 3'b010;

    reg [2:0] State, nextState;

    reg [10:0] wait_cnt = (CLKs_per_BIT + 4)/2;
    reg [3:0]   RX_cnt = 4'd0;

    reg      en_RX_cnt = 1'b0;   
    reg         RX_DV  = 1'b0;

    reg [serial_bits_number + 1 : 0] RX_byte = 0 ; 


    always @(posedge clk) begin
        State <= nextState;
    end


    always @(*) begin
        case (State)

            RST : begin
                if (i_RX_serial == 1'b1) 
                    nextState <= IDLE;
                else
                    nextState <= RST;
            end

            IDLE: begin
                if (i_RX_serial == 1'b0) 
                    nextState <= RX;
                else
                    nextState <= IDLE;
            end  

            RX  : begin
                if (RX_cnt < 10) 
                    nextState <= RX;
                else if (i_RX_serial == 1) 
                        nextState <= IDLE;
                    else 
                        nextState <= RST;
            end

            default: nextState <= RST;

        endcase
    end

    always @( posedge clk) begin

        if (State == IDLE) begin
            wait_cnt  <= (CLKs_per_BIT + 4)/2;	
            en_RX_cnt <= 1'b0;		
        end  

        if (State == RX) begin
            if (wait_cnt < CLKs_per_BIT-1) begin
                wait_cnt  <= wait_cnt + 1;
                en_RX_cnt <= 1'b0;
            end else begin
                wait_cnt  <= 0;
                en_RX_cnt <= 1'b1;
            end
        end
    end

    always @( posedge clk) begin

        if (State == RST) begin                 //! if RX_cnt reach stop bit(RX_cnt = 10), but with the value 1'b0 => data invalid => reset RX_cnt (means no valid data 've been got)
            RX_cnt <= 0;
        end  

        if (State == IDLE) begin
            RX_cnt <= 0;
            if (RX_cnt == 10)                   //! if after LSB we do not get 1'b1 (do not get stop bit) => we go to RST state firts => reset RX_cnt => go to IDLE state => do not assign RX_DV = 1'b1 (due to RX_cnt = 0)
                RX_DV  <= 1'b1;
            else        
                RX_DV  <= 1'b0; 
        end  

        if (en_RX_cnt == 1'b1) begin
            if (RX_cnt == 10) 
                RX_cnt <= 0;
            else 
                RX_cnt <= RX_cnt + 1; 
            
        end
    end

    always @(posedge clk) begin
        if (en_RX_cnt == 1) begin
            RX_byte[9:0] <= {i_RX_serial, RX_byte[serial_bits_number+1:1]};
        end 
    end

    always @(posedge clk) begin
        if (RX_DV == 1) begin
            o_RX_byte[7:0] <= { RX_byte[serial_bits_number:1] };    
        end
    end

    always @(posedge clk) begin
        o_RX_DV <= RX_DV; 
    end

endmodule