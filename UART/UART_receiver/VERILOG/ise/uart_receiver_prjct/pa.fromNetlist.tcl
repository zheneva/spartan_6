
# PlanAhead Launch Script for Post-Synthesis pin planning, created by Project Navigator

create_project -name uart_receiver_prjct -dir "D:/Androsov/spartan_6/UART/UART_receiver/VERILOG/ise/uart_receiver_prjct/planAhead_run_5" -part xc6slx9tqg144-3
set_property design_mode GateLvl [get_property srcset [current_run -impl]]
set_property edif_top_file "D:/Androsov/spartan_6/UART/UART_receiver/VERILOG/ise/uart_receiver_prjct/top_level.ngc" [ get_property srcset [ current_run ] ]
add_files -norecurse { {D:/Androsov/spartan_6/UART/UART_receiver/VERILOG/ise/uart_receiver_prjct} }
set_param project.pinAheadLayout  yes
set_property target_constrs_file "constr_1.ucf" [current_fileset -constrset]
add_files [list {constr_1.ucf}] -fileset [get_property constrset [current_run]]
link_design
