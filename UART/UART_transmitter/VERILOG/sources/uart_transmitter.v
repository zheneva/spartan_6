// CLKS_PER_BIT = (Frequency of i_Clock)/(Frequency of UART)
// Example: 25 MHz Clock, 115200 baud UART
// (25000000)/(115200) = 217

module uart_transmitter 
#(
    parameter  CLKs_per_BIT = 217,
    parameter  serial_bits_number = 8
)
(
    input  clk,
    input  i_TX_DV,
    input [7:0] i_TX_byte,
    output reg o_TX_serial
);
    

   
    localparam [2:0] RST  = 3'b000;
    localparam [2:0] IDLE = 3'b001;
    localparam [2:0] RX   = 3'b010;

    reg [2:0] State, nextState;

    // reg [10:0] wait_cnt = (CLKs_per_BIT + 4)/2;
    reg [10:0] wait_cnt = CLKs_per_BIT;
    reg [3:0]   RX_cnt = 4'd0;

    reg      en_RX_cnt = 1'b0;   
    reg         TX_DV  = 1'b0;

    reg [serial_bits_number - 1 : 0] TX_byte ; 



    always @(posedge clk) begin
        TX_DV   <= i_TX_DV;
        TX_byte <= i_TX_byte;
    end

    always @(posedge clk) begin
        State <= nextState;
    end


    always @(*) begin
        case (State)

            RST : begin
                if (TX_DV == 1'b1)   
                    nextState <= RX;
                else
                    nextState <= RST;
            end

            // IDLE: begin
            //     if (TX_DV == 1'b1) 
            //         nextState <= RX;
            //     else
            //         nextState <= IDLE;
            // end  

            RX  : begin
                if (RX_cnt < 11) 
                    nextState <= RX;
                else 
                    nextState <= RST;
            end

            default: nextState <= RST;

        endcase
    end

    always @( posedge clk) begin

        if (State == RST) begin
            wait_cnt  <= CLKs_per_BIT;	     //! 	 wait_cnt  <= CLKs_per_BIT leeds to no delay between RX_DV bit appers and data to be sampled
            en_RX_cnt <= 1'b0;		
        end  

        if (State == RX) begin
            if (wait_cnt < CLKs_per_BIT-1) begin
                wait_cnt  <= wait_cnt + 1;
                en_RX_cnt <= 1'b0;
            end else begin
                wait_cnt  <= 0;
                en_RX_cnt <= 1'b1;
            end
        end
    end

    always @( posedge clk) begin

        if (State == RST) begin                 //! if RX_cnt reach stop bit(RX_cnt = 10), but with the value 1'b0 => data invalid => reset RX_cnt (means no valid data 've been got)
            RX_cnt <= 0;
        end  

        // if (State == RST) begin
        //     RX_cnt <= 0;
        //     if (RX_cnt == 10)                   //! if after LSB we do not get 1'b1 (do not get stop bit) => we go to RST state firts => reset RX_cnt => go to IDLE state => do not assign RX_DV = 1'b1 (due to RX_cnt = 0)
        //         RX_DV  <= 1'b1;
        //     else        
        //         RX_DV  <= 1'b0; 
        // end  

        if (en_RX_cnt == 1'b1) begin
            if (RX_cnt == 11) 
                RX_cnt <= 0;
            else 
                RX_cnt <= RX_cnt + 1; 
            
        end
    end

    always @(posedge clk) begin
        if (en_RX_cnt == 1) begin
            case (RX_cnt)
                'd0: o_TX_serial <= 0;
                'd1: o_TX_serial <= TX_byte[RX_cnt-1];
                'd2: o_TX_serial <= TX_byte[RX_cnt-1];
                'd3: o_TX_serial <= TX_byte[RX_cnt-1];
                'd4: o_TX_serial <= TX_byte[RX_cnt-1];
                'd5: o_TX_serial <= TX_byte[RX_cnt-1];
                'd6: o_TX_serial <= TX_byte[RX_cnt-1];
                'd7: o_TX_serial <= TX_byte[RX_cnt-1];
                'd8: o_TX_serial <= TX_byte[RX_cnt-1];
                'd9: o_TX_serial <= 1;
                'd10: o_TX_serial <= 1'bz;
            endcase
        end 
    end

endmodule

//ToDo загнать i_TX_byte в регистр RX_byte и устанавливать его только по сигналу i_TX_DV 