


module clk_div (

    input i_clk,
    input clr,
    output o_clk
);
    

    reg [1:0] div_cnt = 0;
    reg clk           = 1'b0;

    // always @(posedge i_clk) begin
    //     if (clr == 1'b1) 
    //         div_cnt <= 0;
    //     else begin
    //         if (div_cnt == 1) begin
    //             div_cnt <= 0;
    //             clk <= ~ clk;
    //         end else
    //             div_cnt <= div_cnt + 1;
    //     end

    // end

    always @(posedge i_clk) begin
    if (clr == 1'b1) 
        clk <= 0;
    else 
         clk <= ~ clk;
    end


// always @(posedge i_clk) begin
//     o_clk <= clk;
// end


assign o_clk = clk;


endmodule