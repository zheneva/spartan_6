

module vga_top_level #(
    parameter  H_pixel   = 640 ,
    parameter  V_lines   = 480 ,
    parameter  H_f_porch = 16  ,
    parameter  H_b_porch = 48  ,
    parameter  H_s_pulse = 96  ,
    parameter  V_f_porch = 10  ,
    parameter  V_b_porch = 29  ,
    parameter  V_s_pulse = 2   ,
    parameter  Respones_delay = 1
) (
    input clk_50MHz,
    input clr,
    input [1:0] mode,
    output vga_hs, 
    output vga_vs,
    output vga_r,
    output vga_g,
    output vga_b,
	 output reg no_bip
);


wire clk_25MHz;




//clk_div clock_div(
//
//      .i_clk(clk_50MHz)
//    , .clr(clr)
//   , .o_clk(clk_25MHz)
//);

clk_wiz instance_name
   (// Clock in ports
    .CLK_50MHz(clk_50MHz),      // IN
    // Clock out ports
    .CLK_25MHz(clk_25MHz)
); 


vga_controller #(
      .H_pixel(H_pixel)
    , .V_lines(V_lines)
    , .H_f_porch(H_f_porch)
    , .H_b_porch(H_b_porch)
    , .H_s_pulse(H_s_pulse)
    , .V_f_porch(V_f_porch)
    , .V_b_porch(V_b_porch)
    , .V_s_pulse(V_s_pulse)
    , .Respones_delay(Respones_delay)
) vga_cntrl (
      .clk_25MHz(clk_25MHz)
    , .clr(clr)
    , .mode(mode)
    , .vga_hs(vga_hs)
    , .vga_vs(vga_vs)
    , .vga_r(vga_r)
    , .vga_g(vga_g)
    , .vga_b(vga_b)
);


always @(posedge clk_25MHz)begin
	no_bip <= 1'b1;
end
  
endmodule