


module pattern_gen #(
    parameter H_pixel = 640,
    parameter V_lines = 480
)(
    input clk_25MHz,
    input clr,
    input [1:0] mode,
    input en_video,
    output reg vga_r, 
    output reg vga_g,
    output reg vga_b
);  
    reg [10:0] x_pos = 0;
    reg [10:0] y_pos = 0;
    reg en_y_pos = 0;

    reg en_change = 0;

        always @(posedge clk_25MHz) begin
            if (clr == 1) begin
                x_pos <= 0;
            end else begin
                if (en_video == 1) begin
                    if (x_pos < H_pixel-1) begin
                        x_pos <= x_pos + 1;
                    end else begin
                        x_pos <= 0;
                    end
                end
            end
        end   

        always @(posedge clk_25MHz) begin
            if (x_pos == H_pixel - 2) 
                en_y_pos <= 1;
            else 
                en_y_pos <= 0;
        end

        always @(posedge clk_25MHz) begin
            if (clr == 1) begin
                y_pos <= 0;
            end else begin
                if (en_y_pos == 1) begin
                    if (y_pos < V_lines-1) 
                        y_pos <= y_pos + 1;
                    else
                        y_pos <= 0;
                end
            end
        end

        always @(posedge clk_25MHz) begin

            case (mode)
                1: begin
                    if (en_video == 1) begin
                        if (x_pos[6] == 1)  begin
                            vga_r <= 1;
                            vga_g <= 0;
                            vga_b <= 1;
                        end else begin
                            vga_r <= 0;
                            vga_g <= 1;
                            vga_b <= 0;
                        end
                    end else begin
                            vga_r <= 0;
                            vga_g <= 0;
                            vga_b <= 0;
                    end
                end

                2: begin
                    if (en_video == 1) begin
                        if (y_pos[6] == 1)  begin
                            vga_r <= 0;
                            vga_g <= 1;
                            vga_b <= 0;
                        end else begin
                            vga_r <= 1;
                            vga_g <= 0;
                            vga_b <= 1;
                        end
                    end else begin
                            vga_r <= 0;
                            vga_g <= 0;
                            vga_b <= 0;
                    end
                end
                
                3: begin
                    if (en_video == 1) begin
                        if (y_pos[6] == 1)  begin
                                if (x_pos[6] == 1)  begin
                                    vga_r <= 1;
                                    vga_g <= 0;
                                    vga_b <= 1;
                                end else begin
                                    vga_r <= 0;
                                    vga_g <= 1;
                                    vga_b <= 0;
                                end
                        end else begin
                                if (x_pos[6] == 1)  begin
                                    vga_r <= 0;
                                    vga_g <= 1;
                                    vga_b <= 0;
                                end else begin
                                    vga_r <= 1;
                                    vga_g <= 0;
                                    vga_b <= 1;
                                end     
                        end
                    end else begin
                            vga_r <= 0;
                            vga_g <= 0;
                            vga_b <= 0;
                    end
                end
            endcase
            
        end


//ToDo добавить case на различные режимы

endmodule