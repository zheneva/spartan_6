


module vga_top_level_test (
);
  

reg clk_50MHz = 1'b0;
wire clr;
wire vga_hs;
wire vga_vs;
wire vga_r;
wire vga_g;
wire vga_b;
always begin
    #10; clk_50MHz <= ~clk_50MHz;
end


vga_top_level #(
        .H_pixel  ( 640 ) 
    ,   .V_lines  ( 480 ) 
    ,   .H_f_porch( 16  ) 
    ,   .H_b_porch( 48  ) 
    ,   .H_s_pulse( 96  ) 
    ,   .V_f_porch( 10  ) 
    ,   .V_b_porch( 29  ) 
    ,   .V_s_pulse( 2   ) 
)vga_top(
        .clk_50MHz(clk_50MHz)
    ,   .clr(clr)
    ,   .vga_hs(vga_hs) 
    ,   .vga_vs(vga_vs)
    ,   .vga_r(vga_r)
    ,   .vga_g(vga_g)
    ,   .vga_b(vga_b)
);


endmodule