

module vga_controller_test (
);
    
    reg clk = 0;
    reg clr = 0;
    wire vga_hs;
    wire vga_vs;
    wire [1:0] mode;
    wire vga_r;
    wire vga_g;
    wire vga_b;

always begin
    #20; clk <= ~clk;
end

assign mode = 3;

vga_controller #(
        .H_pixel  ( 640 ) 
    ,   .V_lines  ( 480 ) 
    ,   .H_f_porch( 16  ) 
    ,   .H_b_porch( 48  ) 
    ,   .H_s_pulse( 96  ) 
    ,   .V_f_porch( 10  ) 
    ,   .V_b_porch( 29  ) 
    ,   .V_s_pulse( 2   ) 
)vga_contrl(
        .clk_25MHz(clk)
    ,   .clr(clr)
    ,   .mode(mode)
    ,   .vga_hs(vga_hs) 
    ,   .vga_vs(vga_vs)
    ,   .vga_r(vga_r)
    ,   .vga_g(vga_g)
    ,   .vga_b(vga_b)
);

endmodule