
# PlanAhead Launch Script for Pre-Synthesis Floorplanning, created by Project Navigator

create_project -name vga_top_project -dir "D:/Androsov/spartan_6/VGA/Verilog/ise/vga_top_project/planAhead_run_4" -part xc6slx9tqg144-3
set_param project.pinAheadLayout yes
set srcset [get_property srcset [current_run -impl]]
set_property target_constrs_file "vga_top_level.ucf" [current_fileset -constrset]
set hdlfile [add_files [list {ipcore_dir/clk_wiz.v}]]
set_property file_type Verilog $hdlfile
set_property library work $hdlfile
set hdlfile [add_files [list {../../sources/pattern_gen.v}]]
set_property file_type Verilog $hdlfile
set_property library work $hdlfile
set hdlfile [add_files [list {../../sources/vga_controller.v}]]
set_property file_type Verilog $hdlfile
set_property library work $hdlfile
set hdlfile [add_files [list {../../sources/vga_top_level.v}]]
set_property file_type Verilog $hdlfile
set_property library work $hdlfile
set_property top vga_top_level $srcset
add_files [list {vga_top_level.ucf}] -fileset [get_property constrset [current_run]]
open_rtl_design -part xc6slx9tqg144-3
