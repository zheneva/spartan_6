-- Signal 1680 x 1050 @ 60 Hz timing

-- Horizontal timing (line)                            Vertical timing (frame)

-- Scanline part   | Pixels   | Time [µs]               Frame part	  | Lines   | Time [ms]
-- Visible area	   | 1680	  | 11.417697431018         Visible area  | 1050	| 16.098953377735
-- Front porch	   | 104	  | 0.70680984096779        Front porch	  | 1	    | 0.015332336550224
-- Sync pulse	   | 184	  | 1.2505097186353         Sync pulse	  | 3	    | 0.045997009650673
-- Back porch	   | 288	  | 1.9573195596031         Back porch	  | 33	    | 0.5059671061574
-- Whole line	   | 2256	  | 15.332336550224         Whole frame	  | 1087	| 16.666249830094

----------------------------------------------------------------------------------------------

-- Signal 640 x 480 @ 60 Hz timing

-- Horizontal timing (line)                         Vertical timing (frame)

-- Scanline part   | Pixels   | Time [µs]               Frame part	  | Lines | Time [ms]
-- Visible area	   | 640	  | 11.417697431018         Visible area  | 480	  | 16.098953377735
-- Front porch	   | 16 	  | 0.70680984096779        Front porch	  | 10    | 0.015332336550224
-- Sync pulse	   | 128 	  | 1.2505097186353         Sync pulse	  | 2	  | 0.045997009650673
-- Back porch	   | 16 	  | 1.9573195596031         Back porch	  | 29	  | 0.5059671061574
-- Whole line	   | 800	  | 15.332336550224         Whole frame	  | 521   | 16.666249830094


library IEEE;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;

entity VGA_top_level is
port (
      PLL_clk_in : in std_logic;
           clr   : in std_logic;
        vga_hs   : out std_logic;
        vga_vs   : out std_logic;
        vga_r    : out std_logic_vector(4 downto 0);
        vga_g    : out std_logic_vector(5 downto 0);
        vga_b    : out std_logic_vector(4 downto 0)
        );
end VGA_top_level;

architecture RTL of VGA_top_level is

component  VGA_CONTROL_gen  
generic(
        H_pixel   : integer := 1680; -- 640; -- 1680
        V_lines   : integer := 1050; -- 480; -- 1050
        H_f_porch : integer := 104;  -- 16 ; -- 104
        H_b_porch : integer := 288;  -- 48 ; -- 288
        H_s_pulse : integer := 184;  -- 96;  -- 184
        V_f_porch : integer := 1;    -- 10 ; -- 1
        V_b_porch : integer := 33;   -- 29 ; -- 33
        V_s_pulse : integer := 3     -- 2    -- 3

  );
       port(
            clk, clr : in std_logic;
            vga_hs   : out std_logic;
            vga_vs   : out std_logic;
            vga_r    : out std_logic_vector(4 downto 0);
            vga_g    : out std_logic_vector(5 downto 0);
            vga_b    : out std_logic_vector(4 downto 0)
      );
end component;


component VGA_CONTROL
  port(
    clk, clr : in std_logic;
    vga_hs   : out std_logic;
    vga_vs   : out std_logic;
    vga_r    : out std_logic_vector(4 downto 0);
    vga_g    : out std_logic_vector(5 downto 0);
    vga_b    : out std_logic_vector(4 downto 0)
  );
end component;

component clk_wiz_0
port
 (-- Clock in ports
  -- Clock out ports
  PLL_25MHz        : out    std_logic;
  PLL_147MHz       : out    std_logic;
  PLL_in           : in     std_logic
 );
end component;


  signal PLL_out   : std_logic;

  
begin

--circ_PLL : clk_wiz_0
--               port map ( 
--                   PLL_out => PLL_out,
--                   PLL_in => PLL_clk_in
--                );
             
circ_PLL : clk_wiz_0
   port map ( 
          -- Clock out ports  
           PLL_25MHz => open,
           PLL_147MHz => PLL_out,
           -- Clock in ports
           PLL_in => PLL_clk_in
 );
 
-- circ_vga: VGA_CONTROL
--               port map(
--                 clk    => PLL_out,
--                 clr    => clr,
--                 vga_hs => vga_hs,
--                 vga_vs => vga_vs,
--                 vga_r  => vga_r,
--                 vga_g  => vga_g,
--                 vga_b  => vga_b  
--               );
               
               
circ_vga_gen: VGA_CONTROL_gen  
                      port map(
                           clk    => PLL_out,
                            clr    => clr,
                           vga_hs => vga_hs,
                           vga_vs => vga_vs,
                           vga_r  => vga_r,
                           vga_g  => vga_g,
                           vga_b  => vga_b
                     );
end RTL;