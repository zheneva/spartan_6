library IEEE;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.std_logic_unsigned.all;

entity Pattern_gen is
  generic(
        mode : boolean := false;
        H_b_porch : integer ;
        H_s_pulse : integer ;
        H_area    : integer ;
        
        
        
        V_s_pulse : integer ;
        V_b_porch : integer ;
        V_area    : integer 
);
        
  port (
     --vidon : in std_logic;
     x_pos : in std_logic_vector(11 downto 0);               -- horizontal counter 
     y_pos : in std_logic_vector(11 downto 0);               -- vertical   counter
  r_chanel : out std_logic_vector(4 downto 0);
  g_chanel : out std_logic_vector(5 downto 0);
  b_chanel : out std_logic_vector(4 downto 0)
  ) ;
end Pattern_gen;
   
architecture Pattern_gen_test of Pattern_gen is

  constant delta : unsigned(4 downto 0) := "10000";  -- 40

inferring  signal flag_1 : std_logic;  
  signal flag_2 : std_logic;
  signal flag_3 : std_logic;
  signal r_chanel_tmp : unsigned(4 downto 0) := (others => '1');
  signal g_chanel_tmp : unsigned(5 downto 0) := (others => '0'); 
  signal b_chanel_tmp : unsigned(4 downto 0) := (others => '0');
  signal RGB_chanels_tmp : unsigned(15 downto 0) := (others => '0');


  signal x_pos_tmp :  std_logic_vector(11 downto 0);
  signal y_pos_tmp :  std_logic_vector(11 downto 0);

  signal inc_enable   : std_logic := '0';

begin

--inc_enable <= '1' when x_pos = "0000000000" else '0';


-- gradient: if(not mode) generate 

--     process(clk)
--     begin
--       if (rising_edge(clk) ) then 
--         if (vidon = '1' and x_pos = 145) then
--           inc_enable <= '1';
--         else
--           inc_enable <= '0';
--         end if;
--       end if;
--     end process;
    
--     flag_1 <= '1' when ((y_pos < 192) and (y_pos > 31)) else '0';
--     flag_2 <= '1' when ((y_pos < 352) and (y_pos > 191)) else '0';
--     flag_3 <= '1' when ((y_pos < 512) and (y_pos > 351)) else '0';
--     process(clk)
--     begin
--       if (rising_edge(clk)) then     
--         if (inc_enable = '1' and y_pos(1 downto 0) = "11" ) then
--           if (flag_1 = '1') then
--              if(r_chanel_tmp = "00000") then 
--                 r_chanel_tmp <= (others => '0');
--                 g_chanel_tmp <= (4 downto 0 => '1', others => '0');

--             else
--                 b_chanel_tmp <= (others => '0');
--                 r_chanel_tmp <= r_chanel_tmp - 1 ;
--                 g_chanel_tmp <= g_chanel_tmp + 1 ;
--             end if;

--           elsif (flag_2 = '1') then
--               if (g_chanel_tmp = "000000") then
--                 g_chanel_tmp <= (others => '0');
--                 b_chanel_tmp <= (others => '1');    
--               else 
--                 r_chanel_tmp <= (others => '0');
--                 g_chanel_tmp <= g_chanel_tmp - 1; 
--                 b_chanel_tmp <= b_chanel_tmp + 1;
--               end if;
--           elsif (flag_3 = '1') then
--             if (b_chanel_tmp = "00000") then
--               b_chanel_tmp <= (others => '0');
--               r_chanel_tmp <= (others => '1');   
--             else
--               g_chanel_tmp <= (others => '0');
--               b_chanel_tmp <= b_chanel_tmp -1;
--               r_chanel_tmp <= r_chanel_tmp + 1;
--             end if;
--           end if;
--         end if;
--       end if;
--     end process;

--   r_chanel <= std_logic_vector(r_chanel_tmp);
--   g_chanel <= std_logic_vector(g_chanel_tmp);
--   b_chanel <= std_logic_vector(b_chanel_tmp);  
 


-- end generate gradient;

x_pos_tmp <= x_pos - H_b_porch - H_s_pulse;
y_pos_tmp <= y_pos - V_b_porch - V_s_pulse;

Chess_board: if (mode) generate

    process(x_pos_tmp, y_pos_tmp)                                             -- Chess board
    begin 
          if (y_pos_tmp(5) = '1') then
              r_chanel <= (others => x_pos_tmp(5));
              g_chanel <= (others => not x_pos_tmp(5));
          else
              r_chanel <= (others => not x_pos_tmp(5));
              g_chanel <= (others => x_pos_tmp(5));
          end if;
    end process;
    
    b_chanel <= (others => '0');
end generate Chess_board;

pixel_frame: if (not mode) generate

process(x_pos_tmp, y_pos_tmp)                                            -- Chess board
  begin 
    if (x_pos_tmp = 0 or y_pos_tmp = 0 or x_pos_tmp = H_area-1 or y_pos_tmp = V_area-1) then  -- visible area(480 or 640) - 1 due to 
     r_chanel <= (others => '1');                                                              -- we start count from 0 -> 0 to 639 = 640px
     g_chanel <= (others => '1');
     b_chanel <= (others => '1');
   else
     r_chanel <= (others => '0');
     g_chanel <= (others => '0');
     b_chanel <= (others => '0');
   end if;
end process;
end generate pixel_frame;
end Pattern_gen_test ; -- Pattern_gen_test

