onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate /core_test/cr/clk
add wave -noupdate /core_test/cr/DIN
add wave -noupdate /core_test/cr/we
add wave -noupdate /core_test/cr/front_we
add wave -noupdate /core_test/cr/d_DIN
add wave -noupdate /core_test/cr/ram_out_1
add wave -noupdate /core_test/cr/ram_out_2
add wave -noupdate /core_test/cr/ram_out_3
add wave -noupdate /core_test/cr/ram_out_4
add wave -noupdate /core_test/cr/ram_out_5
add wave -noupdate /core_test/cr/ram_out_6
add wave -noupdate /core_test/cr/ram_out_7
add wave -noupdate /core_test/cr/ram_out_8
add wave -noupdate /core_test/cr/ram_out_9
add wave -noupdate /core_test/cr/cnv/sum_out
add wave -noupdate /core_test/cr/BUSY
add wave -noupdate /core_test/cr/BUSY_cnt
add wave -noupdate /core_test/cr/line_cnt
add wave -noupdate /core_test/cr/en_cnv
add wave -noupdate /core_test/cr/cnvOUT
add wave -noupdate /core_test/cr/coreOUT
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {1817 ns} 0} {{Cursor 2} {11940 ns} 0}
quietly wave cursor active 1
configure wave -namecolwidth 100
configure wave -valuecolwidth 39
configure wave -justifyvalue left
configure wave -signalnamewidth 1
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ns
update
WaveRestoreZoom {25 ns} {2357 ns}
