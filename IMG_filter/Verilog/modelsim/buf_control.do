onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate /buf_control_test/bf_c/clk
add wave -noupdate /buf_control_test/bf_c/clr
add wave -noupdate /buf_control_test/bf_c/pixl_cnt
add wave -noupdate /buf_control_test/bf_c/en_line_cnt
add wave -noupdate /buf_control_test/bf_c/line_cnt
add wave -noupdate /buf_control_test/bf_c/en_hor_ADDR_cnt
add wave -noupdate /buf_control_test/bf_c/en_ver_ADDR_cnt
add wave -noupdate /buf_control_test/bf_c/hor_ADDR_cnt
add wave -noupdate /buf_control_test/bf_c/ver_ADDR_cnt
add wave -noupdate /buf_control_test/bf_c/ADDR
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {3222 ns} 0}
quietly wave cursor active 1
configure wave -namecolwidth 150
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 1
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ns
update
WaveRestoreZoom {3203 ns} {5311 ns}
