


module core #(
        parameter   ProcDelay = 3             //* defines the number of sycles to rest -> for window trasition to next line
    ,   parameter   H_pixel   = 8 //! = 640 
    ,   parameter   H_f_porch = 1  
    ,   parameter   H_b_porch = 1  

    ,   parameter   V_lines   = 8 
    ,   parameter   V_f_porch = 1 
    ,   parameter   V_b_porch = 1 

    ,   parameter   ram_depth = 10            //* must acccomadate all sagnificant pixels + extra pixles | *exmple line = 8 pixles while filter matrix is 3x3 --> ram depth = 1 + 8 + 1 (1 = 3/2)
    ,   parameter   ram_width = 8
) (
        input clk
    ,   input we
    ,   input [ram_width-1:0] DIN
    ,   output       BUSY
    ,   output [7:0] rADDR
    ,   output [7:0] wADDR
    ,   output       weRAM
    ,   output [16:0] coreOUT
);
    


    
    wire  [7:0] ram_out_1;
    wire  [7:0] ram_out_2;
    wire  [7:0] ram_out_3;
    wire  [7:0] ram_out_4;
    wire  [7:0] ram_out_5;
    wire  [7:0] ram_out_6;
    wire  [7:0] ram_out_7;
    wire  [7:0] ram_out_8;
    wire  [7:0] ram_out_9;

    

    reg [2:0] shift      = 0;
    reg       ch_chernel = 0;


    reg [ram_width-1:0] d_DIN = 0;

//<--------------------------------------------------------------------------------------------------->//

reg prev_we;

    always @(posedge clk) begin                     
        prev_we <= we;                                          //* since we catch incoming enable signal(clr or we) wich leads to one cycle delay
        d_DIN <= DIN;                                           //* we also delay incoming data
    end
wire front_we;

assign front_we = ~prev_we & we;                                //* catch incoming enable signal




    reg [7:0] BUSY_cnt = 8'b0000_0000;
    reg       s_BUSY   = 1'b0;
    reg       d_BUSY   = 1'b0;

    reg [7:0] line_cnt = 8'b0000_0000;
    reg    en_line_cnt = 1'b0;


    always @(posedge clk) begin

        if (d_BUSY == 1'b1) begin                      
            if (BUSY_cnt == ((ProcDelay*(H_b_porch+H_pixel+H_f_porch) + ProcDelay + H_pixel))) begin   //* count till get idle lines wile buffer init + first valid line 
                BUSY_cnt <= ((ProcDelay*(H_b_porch+H_pixel+H_f_porch)) + ProcDelay -1);                //* afterward reset counter to  idle lines number and enable line cnt
                en_line_cnt <= 1'b1;                                                                   //* repeat this till d_BUSY rest to 0  *when line counter get all lines
            end else begin
                BUSY_cnt <= BUSY_cnt + 8'b0000_0001;
                en_line_cnt <= 1'b0;
            end    
        end else begin
                BUSY_cnt    <= 8'b0000_0000;                        
                en_line_cnt <= 1'b0;
        end
    end


    always @(posedge clk) begin

        if (front_we == 1'b1) begin                              //* get data valid bit
            s_BUSY   <= 1'b1;                                    //* pass(assign) catched incoming enable signal(clr or we)
        end  

        d_BUSY <= s_BUSY;                                        //* delay s_BUSY due to RAM responce delay 

        if (en_line_cnt) begin
            if (line_cnt < V_lines -1) begin                     //* vertical counter -> calculate sign lines  -> vertical dimension 
                line_cnt  <= line_cnt + 1;
            end else begin
                line_cnt  <= 8'b0000_0000;  
                s_BUSY    <= 1'b0;                               //* reset s_BUSY signals due to all lines were processed
                d_BUSY    <= 1'b0;                               //* reset d_BUSY signals due to all lines were processed
            end
        end
    end

    assign en_cnv = ( (BUSY_cnt > (ProcDelay*(H_b_porch+H_pixel+H_f_porch)+ProcDelay))) ? 1'b1 : 1'b0;    //*  enable signal for convolution | write enable signal for RAM


    reg [7:0] wADDR_cnt;


    always @(posedge clk) begin
        if (d_BUSY == 1'b1) begin
            if (en_cnv == 1'b1) 
                wADDR_cnt <= wADDR_cnt + 8'b0000_0001;           //* calculate adress for output RAM via VGA controller will read performed pixels
        end else    
            wADDR_cnt <=  8'b0000_0000; 
    end

    assign weRAM = en_cnv;
    assign wADDR = wADDR_cnt;
    assign BUSY  = d_BUSY ;

//<--------------------------------------------------------------------------------------------------->//


    // reg [7:0] d_DINrADDR;
    // wire [7:0] DINrADDR;

    // always @(posedge clk) begin
    //     d_DINrADDR <= DINrADDR;
    // end


buffer #(
        .ram_depth(ram_depth)   
    ,   .ram_width(ram_width)
)  bf (
        .clk(clk)
    ,   .we(BUSY)
    ,   .DIN(DIN)
    ,   .data_1(ram_out_1) 
    ,   .data_2(ram_out_2) 
    ,   .data_3(ram_out_3) 
    ,   .data_4(ram_out_4) 
    ,   .data_5(ram_out_5) 
    ,   .data_6(ram_out_6) 
    ,   .data_7(ram_out_7) 
    ,   .data_8(ram_out_8) 
    ,   .data_9(ram_out_9)
);

wire [16:0] cnvOUT;

conv cnv (
        .clk(clk) 
    ,   .en(BUSY)
    ,   .ch_chernel(ch_chernel)
    ,   .shift(shift)
    ,   .info_1(ram_out_1)
    ,   .info_2(ram_out_2)
    ,   .info_3(ram_out_3)
    ,   .info_4(ram_out_4)
    ,   .info_5(ram_out_5)
    ,   .info_6(ram_out_6)
    ,   .info_7(ram_out_7)
    ,   .info_8(ram_out_8)
    ,   .info_9(ram_out_9)
    ,   .cnvOUT(cnvOUT)
);

assign coreOUT = en_cnv == 1'b1 ? cnvOUT : 1'b0;// & enable_cnv;



//* ---------------------------< ADDR control for input RAM >----------------------------*//


reg DVALID = 1'b0;

reg [7:0] valid_data_cnt = 8'b0000_0000;                           //* counter of read pixles

    always @(posedge clk) begin
        if (front_we == 1'b1) begin                                //* caught incoming signal
            DVALID <= 1'b1;                                        //* pass(assign) catched incoming enable signal(clr or we)  
        end  

        if (DVALID == 1'b1) begin
            if (valid_data_cnt == ((V_lines+V_f_porch+V_b_porch)*(H_pixel+H_f_porch+H_b_porch)-1)) begin
                valid_data_cnt <= 8'b0000_0000;                     //* reset counter
                DVALID         <= 1'b0;                             //* reset data vaalid bit
            end else 
                valid_data_cnt <= valid_data_cnt + 8'b0000_0001;
        end
    end   




buf_control #(                                                      
        .H_pixel(H_pixel)                                           
    ,   .V_lines(V_lines)                                           
    ,   .H_f_porch(H_f_porch)   
    ,   .H_b_porch(H_b_porch)   
    ,   .V_f_porch(V_f_porch)   
    ,   .V_b_porch(V_b_porch)   
) bf_c (
        .clk(clk)
    ,   .clr(~DVALID)                                               //* module get incoming enable signal and
    ,   .ADDR(rADDR)                                             //* calculate correct read adress 
);  




// assign rADDR = DINrADDR;
//* ---------------------------< ADDR control for input RAM >----------------------------*//






endmodule