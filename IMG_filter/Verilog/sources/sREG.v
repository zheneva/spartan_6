//     sreg_width = 4
//     sreg_depth = 4
//
//     ___0___ ___1___ ___2___ ___3___     <-- sreg_depth
//    |_MSB:0_|_MSB:0_|_MSB:0_|_MSB:0_|  
//        |      |       |     |
//      [4:0]  [4:0]   [4:0]  [4:0]        <--  sreg_width
//



module sREG #(
    parameter sreg_width = 4
    //,parameter sreg_depth = 3
) (
    input clk,
    input en,
    input [sreg_width-1:0] inData,
    output [sreg_width-1:0] outData_1,
    output [sreg_width-1:0] outData_2,
    output [sreg_width-1:0] outData_3
);
    

    reg [(sreg_width-1):0] sreg [2:0]; 


    always @(posedge clk) begin
        if (en == 1'b1) begin
            sreg[2] <= inData;
            sreg[1] <= sreg[2];
            sreg[0] <= sreg[1];
        end 
            // sreg[2] <= 0;
        
            // sreg[1] <= sreg[2];
            // sreg[0] <= sreg[1];
        // end
    end


    assign outData_1 = sreg[2];
    assign outData_2 = sreg[1];
    assign outData_3 = sreg[0];



endmodule