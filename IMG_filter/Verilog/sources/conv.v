


module conv (
        input clk 
    ,   input en
    ,   input [2:0]shift
    ,   input ch_chernel
    ,   input [7:0] info_1
    ,   input [7:0] info_2
    ,   input [7:0] info_3
    ,   input [7:0] info_4
    ,   input [7:0] info_5
    ,   input [7:0] info_6
    ,   input [7:0] info_7
    ,   input [7:0] info_8
    ,   input [7:0] info_9
    ,   output reg [16:0] cnvOUT
);
    
    wire  [15:0] mult_out_1;
    wire  [15:0] mult_out_2;
    wire  [15:0] mult_out_3;
    wire  [15:0] mult_out_4;
    wire  [15:0] mult_out_5;
    wire  [15:0] mult_out_6;
    wire  [15:0] mult_out_7;
    wire  [15:0] mult_out_8;
    wire  [15:0] mult_out_9;
    wire  [16:0] sum_out;


    reg [7:0] koef_1 = 1;
    reg [7:0] koef_2 = 1;
    reg [7:0] koef_3 = 1;
    reg [7:0] koef_4 = 1;
    reg [7:0] koef_5 = 1;
    reg [7:0] koef_6 = 1;
    reg [7:0] koef_7 = 1;
    reg [7:0] koef_8 = 1;
    reg [7:0] koef_9 = 1;



    always @(posedge clk) begin
        if (ch_chernel == 1'b0) begin
            koef_1 <= 1;
            koef_2 <= 1;
            koef_3 <= 1;
            koef_4 <= 1;
            koef_5 <= 1;
            koef_6 <= 1;
            koef_7 <= 1;
            koef_8 <= 1;
            koef_9 <= 1;
        end else begin
            koef_1 <= 1;
            koef_2 <= 2;
            koef_3 <= 1;
            koef_4 <= 2;
            koef_5 <= 4;
            koef_6 <= 2;
            koef_7 <= 1;
            koef_8 <= 2;
            koef_9 <= 1;
        end
    end


mult m_1 (
        .pixel(info_1)
    ,   .factor(koef_1)
    ,   .mult_out(mult_out_1)
);

mult m_2 (
        .pixel(info_2)
    ,   .factor(koef_2)
    ,   .mult_out(mult_out_2)
);

mult m_3 (
        .pixel(info_3)
    ,   .factor(koef_3)
    ,   .mult_out(mult_out_3)
);

mult m_4 (
        .pixel(info_4)
    ,   .factor(koef_4)
    ,   .mult_out(mult_out_4)
);

mult m_5 (
        .pixel(info_5)
    ,   .factor(koef_5)
    ,   .mult_out(mult_out_5)
);

mult m_6 (
        .pixel(info_6)
    ,   .factor(koef_6)
    ,   .mult_out(mult_out_6)
);

mult m_7 (
        .pixel(info_7)
    ,   .factor(koef_7)
    ,   .mult_out(mult_out_7)
);

mult m_8 (
        .pixel(info_8)
    ,   .factor(koef_8)
    ,   .mult_out(mult_out_8)
);

mult m_9 (
        .pixel(info_9)
    ,   .factor(koef_9)
    ,   .mult_out(mult_out_9)
);
    


sum sm (
        .data_1(mult_out_1)
    ,   .data_2(mult_out_2)
    ,   .data_3(mult_out_3)
    ,   .data_4(mult_out_4)
    ,   .data_5(mult_out_5)
    ,   .data_6(mult_out_6)
    ,   .data_7(mult_out_7)
    ,   .data_8(mult_out_8)
    ,   .data_9(mult_out_9)
    ,   .sum_out(sum_out)
);



always @(posedge clk) begin
    if (en == 1) begin
        cnvOUT <= sum_out>>shift;   
    end
end

endmodule