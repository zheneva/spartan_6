


module buffer #(

        parameter   ram_depth = 10   
    ,   parameter   ram_width = 8
) (
    input clk,
    input we,
    input [ram_width-1:0] DIN,
    output [ram_width-1:0] data_1, 
    output [ram_width-1:0] data_2, 
    output [ram_width-1:0] data_3, 
    output [ram_width-1:0] data_4, 
    output [ram_width-1:0] data_5, 
    output [ram_width-1:0] data_6, 
    output [ram_width-1:0] data_7, 
    output [ram_width-1:0] data_8, 
    output [ram_width-1:0] data_9 
);

    reg [7:0] ADDR = 8'b0000_0000;

    always @(posedge clk) begin
        if (we == 1) begin
            if (ADDR == ram_depth-1 ) 
                ADDR <= 8'b0000_0000;
            else    
                ADDR <= ADDR + 8'b0000_0001;
        end else 
            ADDR = 8'b0000_0000;
    end


    wire [ram_width-1:0] DOUT_1;
    wire [ram_width-1:0] DOUT_2;
    wire [ram_width-1:0] DOUT_3;

singleport_ram #(
        .width(ram_width)
    ,   .depth(ram_depth)
) ram_1 (
        .clk(clk)
    ,   .we(we)
    ,   .ADDR(ADDR)
    ,   .DIN(DIN)
    ,   .DOUT(DOUT_1)
);

sREG #(
        .sreg_width(ram_width)
    //,parameter sreg_depth = 3
) sRG_1 (
        .clk(clk)
    ,   .en(we)
    ,   .inData(DOUT_1)
    ,   .outData_1(data_1)
    ,   .outData_2(data_2)
    ,   .outData_3(data_3)
);

singleport_ram #(
        .width(ram_width)
    ,   .depth(ram_depth)
) ram_2 (
        .clk(clk)
    ,   .we(we)
    ,   .ADDR(ADDR)
    ,   .DIN(DOUT_1)
    ,   .DOUT(DOUT_2)
);

sREG #(
        .sreg_width(ram_width)
    //,parameter sreg_depth = 3
) sRG_2 (
        .clk(clk)
    ,   .en(we)
    ,   .inData(DOUT_2)
    ,   .outData_1(data_4)
    ,   .outData_2(data_5)
    ,   .outData_3(data_6)
);

singleport_ram #(
        .width(ram_width)
    ,   .depth(ram_depth)
) ram_3 (
        .clk(clk)
    ,   .we(we)
    ,   .ADDR(ADDR)
    ,   .DIN(DOUT_2)
    ,   .DOUT(DOUT_3)
);

sREG #(
        .sreg_width(ram_width)
    //,parameter sreg_depth = 3
) sRG_3 (
        .clk(clk)
    ,   .en(we)
    ,   .inData(DOUT_3)
    ,   .outData_1(data_7)
    ,   .outData_2(data_8)
    ,   .outData_3(data_9)
);

endmodule