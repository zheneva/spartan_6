


module singleport_ram #(
    parameter width = 8,
    parameter depth = 5

) (
    input clk,
    input we,
    input [7:0] ADDR,
    input [width-1:0] DIN,
    output reg [width-1:0] DOUT
);
    

    (* RAM_STYLE="BLOCK" *)
    reg [width-1:0] ram_name [0:depth-1]; //= {(width-1){1'b0}}

    integer i;
    initial begin 
        for (i=0; i<depth; i=i+1)
                    ram_name[i] = 0;
    end

    always @(posedge clk) begin
        if (we == 1'b1) begin
            ram_name[ADDR] <= DIN;
        end
    end


    
    always @(negedge clk) begin
        // if (we == 1'b1) begin
            DOUT <= ram_name[ADDR];
        // end
    end


endmodule