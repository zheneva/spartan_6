

module top_level #(
        parameter  ProcDelay = 3 //*3   define the number of sycles to rest -> for window trasition to next line  3
    ,   parameter  H_pixel   = 8 //*8   
    ,   parameter  H_f_porch = 1 //*1    
    ,   parameter  H_b_porch = 1 //*1    
	,   parameter  V_lines   = 6 //*6   
    ,   parameter  V_f_porch = 1 //*1    
    ,   parameter  V_b_porch = 1 //*1   

    ,   parameter   ram_depth = 642  //*     must acccomadate all sagnificant pixels + extra pixles | *exmple line = 8 pixles whilw filter matrix is 3x3 --> ram depth = 1 + 8 + 1 (1 = 3/2)
    ,   parameter   ram_width = 8
) (
        input clk_25MHz
    ,   output [7:0] coreOUT
    // ,   input clr
    // ,   input  [1:0] mode
    // ,   output vga_hs 
    // ,   output vga_vs

);
    

//<----------------- we control---------------->//

reg [19:0] weControl_cnt = 20'b0000_0000_0000_0000;

always @(posedge clk_25MHz) begin
    if (weControl_cnt == 20'b0100_1011_1011_0100_1010)                                                //* 1001011101101001010(2) = 310090(10) = 3(1+642+1)+480(1+480+1)+3+1  due to 640x480 monitor resolution
        weControl_cnt <= 20'b0000_0000_0000_0000;
    else 
        weControl_cnt <= weControl_cnt + 20'b0000_0000_0000_0001;
end

wire weCntrl = (weControl_cnt > 310088) ? 1'b1 : 1'b0;


//<----------------- wire---------------->//


//wire t_en_sreg;
//
//wire [7:0] vga_r;
//wire       vga_g;
//wire       vga_b;
//wire [7:0] ADDR;



/*vga_controller #(
        .H_pixel(H_pixel)
    ,   .V_lines(V_lines)
    ,   .H_f_porch(H_f_porch) 
    ,   .H_b_porch(H_b_porch) 
    ,   .H_s_pulse(H_s_pulse)    //96  , 
    ,   .V_f_porch(V_f_porch) 
    ,   .V_b_porch(V_b_porch) 
    ,   .V_s_pulse(V_s_pulse)  
    ,   .Respones_delay(Respones_delay)
) vga (
        .clk_25MHz(clk_25MHz)
    ,   .clr(clr)
    ,   .mode(mode)
    ,   .vga_hs(vga_hs) 
    ,   .vga_vs(vga_vs)
    ,   .vga_r(vga_r)
    ,   .vga_g(vga_g)
    ,   .vga_b(vga_b)
    ,   .d_en_sreg(t_en_sreg)
);*/


wire [7:0] dina = 8'b0000_0000;
wire [7:0] dout ;
wire [7:0] outramdout;


wire [7:0] rADDR;
wire [7:0] wADDR;
wire 		  weRAM;
wire 		  BUSY ;

dport_ram INdram (
  .clka(clk_25MHz),   // input clka
  .wea(1'b0),          // input [0 : 0] wea
  .addra(rADDR),      // input [7 : 0] addra
  .dina(dina),        // input [7 : 0] dina
  .douta(dout)       // output [7 : 0] douta
);

core #(
        .ProcDelay(ProcDelay)
    ,   .H_pixel(H_pixel)
    ,   .H_f_porch(H_f_porch)  
    ,   .H_b_porch(H_b_porch)  

    ,   .V_lines(V_lines) 
    ,   .V_f_porch(V_f_porch)
    ,   .V_b_porch(V_b_porch)

    ,   .ram_depth(ram_depth)   
    ,   .ram_width(ram_width)
) cr (
        .clk(clk_25MHz)
    ,   .we(weCntrl)
    ,   .DIN(dout)
    ,   .BUSY(BUSY)
    ,   .rADDR(rADDR)
    ,   .wADDR(wADDR)
    ,   .weRAM(weRAM)
    ,   .coreOUT(coreOUT)
);


dport_ram OUTdram (
  .clka(clk_25MHz),   // input clka
  .wea(weRAM),          // input [0 : 0] wea
  .addra(wADDR),      // input [7 : 0] addra
  .dina(coreOUT),        // input [7 : 0] dina
  .douta(outramdout)       // output [7 : 0] douta
);


// always @(posedge clk_25MHz) begin               //  ram response delay
//     d_ADDR <= ADDR;
// end



endmodule