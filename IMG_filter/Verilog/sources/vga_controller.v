//*   Signal 640 x 480 @ 60 Hz timing
//*   Horizontal timing (line)                         Vertical timing (frame)
//*   Scanline part    | Pixels   | Time [µs]               Frame part	  | Lines | Time [ms]
//*   Visible area	   | 640	  | 11.417697431018         Visible area  | 480	  | 16.098953377735
//*   Front porch	   | 16 	  | 0.70680984096779        Front porch	  | 10    | 0.015332336550224
//*   Sync pulse	   | 128 	  | 1.2505097186353         Sync pulse	  | 2	  | 0.045997009650673
//*   Back porch	   | 16 	  | 1.9573195596031         Back porch	  | 29	  | 0.5059671061574
//*   Whole line	   | 800	  | 15.332336550224         Whole frame	  | 521   | 16.666249830094


module vga_controller #(
    parameter  H_pixel   = 640 ,
    parameter  V_lines   = 480 ,
    parameter  H_f_porch = 16  ,
    parameter  H_b_porch = 48  ,
    parameter  H_s_pulse = 96  ,   //96  , 
    parameter  V_f_porch = 10  ,
    parameter  V_b_porch = 29  ,
    parameter  V_s_pulse = 2   ,
    parameter  Respones_delay= 1
) (
    input clk_25MHz,
    input clr,
    input  [1:0] mode,
    output reg vga_hs, 
    output reg vga_vs,
    output reg d_en_sreg,
    output [7:0]vga_r,
    output vga_g,
    output vga_b

);
    reg [18:0] tc = 0;
    reg [18:0] rc = 0;
    reg [10:0] hc = 0;
    reg [10:0] vc = 0;
    // reg en_video  = 0;
    reg en_video_pg  = 0;
    reg en_vc     = 0;

//------------ dualport_ram ----------// 
reg we  = 1'b0;
reg din = 1'b0;
wire     doutb;
// wire and_doutb;
wire     douta;
// wire and_douta;

wire s_vga_hs;
wire s_vga_vs;
wire en_sreg;
wire en_video;

reg d_en_video;
// reg d_en_sreg;
// reg d_vga_vs;
// reg d_vga_hs;

//------------ dualport_ram ----------// 




// pattern_gen #(
//       .H_pixel(640)
//     , .V_lines(480)
// ) PG (
//       .clk_25MHz(clk_25MHz)
//     , .clr(clr)
//     , .mode(mode)
//     , .en_video(en_video_pg)
//     , .vga_r(vga_r) 
//     , .vga_g(vga_g)
//     , .vga_b(vga_b) 
// );
// always @(posedge clk_25MHz) begin
//     if (((hc > (H_s_pulse + H_b_porch - 2 - Respones_delay)) & (hc <( H_s_pulse + H_b_porch + H_pixel-2))) & (vc > ( V_s_pulse + V_b_porch - 1)) & vc < ((V_s_pulse + V_b_porch + V_lines)))
//         en_video_pg <= 1'b1;
//     else 
//         en_video_pg <= 1'b0;
// end




    always @(posedge clk_25MHz) begin
        if (clr == 1) 
            rc <= 0;
        else begin
            if (rc == 307199)
                rc <= 19'b000_0000_0000_0000_0000;
            else if (en_video == 1'b1) begin
                rc <= rc + 19'b000_0000_0000_0000_0001;
            end
        end
    end

    always @(posedge clk_25MHz) begin
        if (clr == 1) begin
            hc <= 0;
        end else begin
            if (hc < H_f_porch + H_b_porch + H_s_pulse + H_pixel -1  ) begin
                hc    <= hc + 1;
                en_vc <= 1'b0;
            end else begin
                hc    <= 0;
                en_vc <= 1'b1;  
            end
        end
    end

    always @(posedge clk_25MHz) begin
        if (clr == 1) begin
            vc <= 0;
        end else begin
            if (en_vc == 1'b1) begin
                if(vc == (V_f_porch + V_b_porch + V_s_pulse + V_lines - 1)) 
                    vc <= 0;
                else 
                    vc <= vc + 1;
            end
        end
    end





    assign s_vga_hs = (hc < H_s_pulse) ? 1'b0 : 1'b1;
    assign s_vga_vs = (vc < V_s_pulse) ? 1'b0 : 1'b1;
    assign en_video = ((hc > (H_s_pulse + H_b_porch -1)) && (hc < (H_s_pulse + H_b_porch + H_pixel)) && (vc >  (V_s_pulse + V_b_porch - 1)) && (vc < (V_s_pulse + V_b_porch + V_lines))) ? 1'b1 : 1'b0;
    assign en_sreg  = ((hc > (H_s_pulse + H_b_porch -1)) && (hc < (H_s_pulse + H_b_porch + H_pixel)));


    always @(posedge clk_25MHz) begin
        d_en_video  <= en_video;
        d_en_sreg   <= en_sreg ;
        vga_vs      <= s_vga_vs;
        vga_hs      <= s_vga_hs;
    end


    assign  vga_r = (d_en_video) ? rc[7:0] : 1'b0;
    assign  vga_b = 1'b0; 
    assign  vga_g = (douta & d_en_video);
    // assign  d_en_sreg = d_en_sreg;

// dualport_ram my_ram (
//   .clka(clk_25MHz), // input clka
//   .wea(we), // input [0 : 0] wea
//   .addra(rc), // input [18 : 0] addra
//   .dina(din), // input [0 : 0] dina
//   .douta(douta), // output [0 : 0] douta
//   .clkb(clk_25MHz), // input clkb
//   .web(we), // input [0 : 0] web
//   .addrb(rc), // input [18 : 0] addrb
//   .dinb(din), // input [0 : 0] dinb
//   .doutb(doutb) // output [0 : 0] doutb
// );

endmodule