

module mult (
        input [7:0] pixel
    ,   input [7:0] factor
    ,   output [15:0]mult_out
);
    


assign mult_out = pixel * factor;



endmodule