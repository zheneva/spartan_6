

module top_level_test #(
        parameter  ProcDelay = 3  //!= 3
    ,   parameter  H_pixel   = 8  //!= 640 
    ,   parameter  H_f_porch = 1  //!= 16  
    ,   parameter  H_b_porch = 1  //!= 48  
    ,   parameter  V_lines   = 6  //!= 480 
    ,   parameter  V_f_porch = 1  //!= 10  
    ,   parameter  V_b_porch = 1  //!= 29  

    ,   parameter   ram_depth = 10   
    ,   parameter   ram_width = 8
) (
);



    
    reg clk;
    // reg clr;
    // reg [1:0] mode;
    // wire vga_hs; 
    // wire vga_vs; 
    wire [7:0] coreOUT;     


always begin
    #20; clk <= ~clk;
end


initial begin


clk = 1'b0;

end


top_level #(
        .ProcDelay(ProcDelay)
    ,   .H_pixel(H_pixel)
    ,   .H_f_porch(H_f_porch)  
    ,   .H_b_porch(H_b_porch)  

    ,   .V_lines(V_lines) 
    ,   .V_f_porch(V_f_porch)
    ,   .V_b_porch(V_b_porch)

    ,   .ram_depth(ram_depth)   
    ,   .ram_width(ram_width)
) tl (
        .clk_25MHz(clk)
    ,   .coreOUT(coreOUT)
    // ,   .clr(clr)
    // ,   .mode(mode)
    // ,   .vga_hs(vga_hs) 
    // ,   .vga_vs(vga_vs)

);
endmodule