

module sum (
        input [15:0]data_1
    ,   input [15:0]data_2
    ,   input [15:0]data_3
    ,   input [15:0]data_4
    ,   input [15:0]data_5
    ,   input [15:0]data_6
    ,   input [15:0]data_7
    ,   input [15:0]data_8
    ,   input [15:0]data_9
    ,   output [16:0]sum_out
);

wire [16:0] s_data_1;
wire [16:0] s_data_2;
wire [16:0] s_data_3;
wire [16:0] s_data_4;
wire [16:0] s_data_5;
wire [16:0] s_data_6;
wire [16:0] s_data_7;
wire [16:0] s_data_8;
wire [16:0] s_data_9;


assign s_data_1 = { 1'b0, data_1 };
assign s_data_2 = { 1'b0, data_2 };
assign s_data_3 = { 1'b0, data_3 };
assign s_data_4 = { 1'b0, data_4 };
assign s_data_5 = { 1'b0, data_5 };
assign s_data_6 = { 1'b0, data_6 };
assign s_data_7 = { 1'b0, data_7 };
assign s_data_8 = { 1'b0, data_8 };
assign s_data_9 = { 1'b0, data_9 };

assign sum_out = s_data_1 + s_data_2 + s_data_3 + s_data_4 + s_data_5 + s_data_6 + s_data_7 + s_data_8 + s_data_9;

    
endmodule