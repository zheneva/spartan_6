

module buf_control #(
        parameter  H_pixel   = 20 //! = 640 
    ,   parameter  V_lines   = 7 //! = 480 
    ,   parameter  H_f_porch = 5 //! = 16  
    ,   parameter  H_b_porch = 5 //! = 48  
    ,   parameter  V_f_porch = 2 //! = 10  
    ,   parameter  V_b_porch = 2 //! = 29  
//*    ,   parameter  H_s_pulse = 9 //! = 96     //96  , 
//*    ,   parameter  V_s_pulse = 2 //! = 2   
) (
        input clk
    ,   input clr
    ,   output [7:0] ADDR
    // ,   input
    // ,   input
    // ,   input
);
    
    // reg [7:0] deADDRLL = (V_lines-1)*H_pixel;
    // reg [7:0] deADDRFL = 8'b0000_0000;




//     reg prev_we;

//     always @(posedge clk) begin                     
//         prev_we <= we;                              //* since we catch incoming enable signal(clr or we) wich leads to one cycle delay
//         d_DIN <= DIN;                               //* we also delay incoming data
//     end

//     wire front_we;

// assign front_we = ~prev_we & we;                    //* catch incoming enable signal


    reg [7:0] pixl_cnt = 8'b0000_0000;
    reg [7:0] line_cnt = 8'b0000_0000;
    reg [7:0] hor_ADDR_cnt = 8'b0000_0000;
    reg [7:0] ver_ADDR_cnt = 8'b0000_0000;

    wire en_line_cnt;
    wire en_hor_ADDR_cnt;
    wire en_ver_ADDR_cnt;

    //!wire [7:0] ADDR;

    always @(posedge clk) begin
        if (clr == 1) begin
            pixl_cnt <= 0;
        end else begin
            if (pixl_cnt < H_f_porch + H_b_porch + H_pixel-1) begin                     //* horisontal counter -> calculate pixels including back/front porches -> horisontal dimension 
                pixl_cnt    <= pixl_cnt + 1;
                //en_line_cnt <= 1'b0;
            end else begin
                pixl_cnt    <= 0;
                //en_line_cnt <= 1'b1;  
            end
        end
    end

assign en_line_cnt = (pixl_cnt == H_f_porch + H_b_porch + H_pixel-1);

    always @(posedge clk) begin
        if (clr == 1) begin
            line_cnt <= 0;
        end else begin
            if (en_line_cnt) begin
                if (line_cnt < V_f_porch + V_b_porch + V_lines-1) begin                 //* vertical counter -> calculate lines including back/front porches -> vertical dimension 
                    line_cnt    <= line_cnt + 1;
                end else begin
                    line_cnt    <= 0;  
                end
            end
        end
    end


assign en_hor_ADDR_cnt = (((pixl_cnt > H_b_porch - 1) && (pixl_cnt < H_b_porch + H_pixel-1)) || (pixl_cnt == H_b_porch + H_pixel + H_f_porch - 1));
assign en_ver_ADDR_cnt = (((line_cnt > V_b_porch - 1) && (line_cnt < V_b_porch + V_lines-1)) || (line_cnt == V_b_porch + V_lines + V_f_porch - 1));     


    always @(posedge clk) begin                                                          
        if (clr == 1) begin
            hor_ADDR_cnt <= 0;
        end else begin
            if (en_hor_ADDR_cnt) begin    
                    if (hor_ADDR_cnt < (H_pixel-1)) begin                           
                        hor_ADDR_cnt    <= hor_ADDR_cnt + 1;                                 //* calculate addr position of pixel on every single line 
                    end else begin
                        hor_ADDR_cnt    <= 0;  
                    end
            end
        end
    end


    always @(posedge clk) begin
        if (clr == 1) begin
            ver_ADDR_cnt <= 0;
        end else begin
            if (en_ver_ADDR_cnt) begin    
                if (en_line_cnt) begin
                    if (ver_ADDR_cnt < (V_lines-1)) begin                           
                        ver_ADDR_cnt    <= ver_ADDR_cnt + 1;                                //* calculate addr position of line on every single frame
                    end else begin
                        ver_ADDR_cnt    <= 0;  
                    end
                end
            end
        end
    end

assign ADDR = (hor_ADDR_cnt + (ver_ADDR_cnt*H_pixel));                                     //* calculate addr for RAM -> pixels in ram do not devide on lines and stores in lage single line


endmodule



//ToDo init hor_ADDR_cnt with 27