

module buf_control_test #(
        parameter  H_pixel   = 8 //! = 640 
    ,   parameter  V_lines   = 4 //! = 480 
    ,   parameter  H_f_porch = 1 //! = 16  
    ,   parameter  H_b_porch = 1 //! = 48  
    ,   parameter  V_f_porch = 1 //! = 10  
    ,   parameter  V_b_porch = 1 //! = 29  
)();
    




reg clk;
reg clr;
wire [7:0] ADDR;


always begin
    #20; clk <= ~clk;
end


initial begin

    clk = 1'b0;
    clr = 1'b0;



    clr <= 1'b1; #3980;
    clr <= 1'b0; #2360; //!#9960;
    clr <= 1'b1; #40000;
end



buf_control #(
        .H_pixel(H_pixel) //! = 640 
    ,   .V_lines(V_lines) //! = 480 
    ,   .H_f_porch(H_f_porch) //! = 16  
    ,   .H_b_porch(H_b_porch) //! = 48  
    ,   .V_f_porch(V_f_porch) //! = 10  
    ,   .V_b_porch(V_b_porch) //! = 29  

) bf_c (
        .clk(clk)
    ,   .clr(clr)
    ,   .ADDR(ADDR)
);



endmodule