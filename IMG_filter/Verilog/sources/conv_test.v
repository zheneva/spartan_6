


module conv_test (
);
    
    reg clk;    
    reg en;
    reg [2:0]shift;
    reg ch_chernel;
    reg [7:0] info_1 = 0;
    reg [7:0] info_2 = 0;
    reg [7:0] info_3 = 0;
    reg [7:0] info_4 = 0;
    reg [7:0] info_5 = 0;
    reg [7:0] info_6 = 0;
    reg [7:0] info_7 = 0;
    reg [7:0] info_8 = 0;
    reg [7:0] info_9 = 0;
    wire [16:0] cnvOUT ;


initial begin
    clk = 0;
    en = 1;
    shift = 2;
    ch_chernel = 1;
    info_1 = 4'b0000; info_2 = 4'b0000; info_3 = 4'b0000; info_4 = 4'b0000; info_5 = 4'b0000; info_6 = 4'b0000; info_7 = 4'b0000; info_8 = 4'b0000; info_9 = 4'b0000;


    info_1 = 4'b0000; info_2 = 4'b0000; info_3 = 4'b0000; info_4 = 4'b0000; info_5 = 4'b0000; info_6 = 4'b0000; info_7 = 4'b0000; info_8 = 4'b0000; info_9 = 4'b0000; #40;

    info_1 = 4'b0001; info_2 = 4'b0001; info_3 = 4'b0001; info_4 = 4'b0001; info_5 = 4'b0001; info_6 = 4'b0001; info_7 = 4'b0001; info_8 = 4'b0001; info_9 = 4'b0001; #40;

    info_1 = 4'b0001; info_2 = 4'b0001; info_3 = 4'b0001; info_4 = 4'b0001; info_5 = 4'b0010; info_6 = 4'b0001; info_7 = 4'b0001; info_8 = 4'b0001; info_9 = 4'b0001; #40;

    info_1 = 4'b0001; info_2 = 4'b0001; info_3 = 4'b0001; info_4 = 4'b0001; info_5 = 4'b0011; info_6 = 4'b0001; info_7 = 4'b0001; info_8 = 4'b0001; info_9 = 4'b0001; #40;

    info_1 = 4'b0001; info_2 = 4'b0001; info_3 = 4'b0001; info_4 = 4'b0001; info_5 = 4'b0100; info_6 = 4'b0001; info_7 = 4'b0001; info_8 = 4'b0001; info_9 = 4'b0001; #40;

    info_1 = 4'b0001; info_2 = 4'b0001; info_3 = 4'b0001; info_4 = 4'b0001; info_5 = 4'b0101; info_6 = 4'b0001; info_7 = 4'b0001; info_8 = 4'b0001; info_9 = 4'b0001; #40;

    info_1 = 4'b0001; info_2 = 4'b0001; info_3 = 4'b0001; info_4 = 4'b0001; info_5 = 4'b0110; info_6 = 4'b0001; info_7 = 4'b0001; info_8 = 4'b0001; info_9 = 4'b0001; #40;

    info_1 = 4'b0001; info_2 = 4'b0001; info_3 = 4'b0001; info_4 = 4'b0001; info_5 = 4'b0111; info_6 = 4'b0001; info_7 = 4'b0001; info_8 = 4'b0001; info_9 = 4'b0001; #40;

    info_1 = 4'b0001; info_2 = 4'b0001; info_3 = 4'b0001; info_4 = 4'b0001; info_5 = 4'b1000; info_6 = 4'b0001; info_7 = 4'b0001; info_8 = 4'b0001; info_9 = 4'b0001; #40;

end


always begin
    #20; clk <= ~clk;
end


conv cnv (
        .clk(clk) 
    ,   .en(en)
    ,   .shift(shift)
    ,   .ch_chernel(ch_chernel)
    ,   .info_1(info_1)
    ,   .info_2(info_2)
    ,   .info_3(info_3)
    ,   .info_4(info_4)
    ,   .info_5(info_5)
    ,   .info_6(info_6)
    ,   .info_7(info_7)
    ,   .info_8(info_8)
    ,   .info_9(info_9)
    ,   .cnvOUT(cnvOUT)
);


endmodule