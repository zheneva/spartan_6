library IEEE;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity CORE is
  port (
      clk  : in std_logic;
      w_en : in std_logic;
      cnv_en : in std_logic;
      ch_cernel : in std_logic;
    inDATA : in std_logic_vector(4 downto 0);
    shift  : in natural;
    coreOUT : out std_logic_vector(4 downto 0)
  );
end CORE;

architecture CORE_strucT of CORE is

component CONV 
  port (
      clk    : in std_logic;
      en     : in std_logic;
      shift  : in natural;
      ch_cernel : in std_logic;
      info_1 : in std_logic_vector(4 downto 0);
      info_2 : in std_logic_vector(4 downto 0);
      info_3 : in std_logic_vector(4 downto 0);
      info_4 : in std_logic_vector(4 downto 0);
      info_5 : in std_logic_vector(4 downto 0);
      info_6 : in std_logic_vector(4 downto 0);
      info_7 : in std_logic_vector(4 downto 0);
      info_8 : in std_logic_vector(4 downto 0);
      info_9 : in std_logic_vector(4 downto 0);
      cnvOUT : out std_logic_vector(4 downto 0)
  );
end component;

component BUF 
    generic(
        b_porch : integer := 3;
        h_pixel : integer := 642
    );
    port (
           clk : in std_logic;
           w_en: in std_logic;
        inDATA : in std_logic_vector(4 downto 0);
        Data_1 : out std_logic_vector(4 downto 0);
        Data_2 : out std_logic_vector(4 downto 0);
        Data_3 : out std_logic_vector(4 downto 0);
        Data_4 : out std_logic_vector(4 downto 0);
        Data_5 : out std_logic_vector(4 downto 0);
        Data_6 : out std_logic_vector(4 downto 0);
        Data_7 : out std_logic_vector(4 downto 0);
        Data_8 : out std_logic_vector(4 downto 0);
        Data_9 : out std_logic_vector(4 downto 0)
    );
end component;


signal  Data_1 : std_logic_vector(4 downto 0) := (others => '0');
signal  Data_2 : std_logic_vector(4 downto 0) := (others => '0');
signal  Data_3 : std_logic_vector(4 downto 0) := (others => '0');
signal  Data_4 : std_logic_vector(4 downto 0) := (others => '0');
signal  Data_5 : std_logic_vector(4 downto 0) := (others => '0');
signal  Data_6 : std_logic_vector(4 downto 0) := (others => '0');
signal  Data_7 : std_logic_vector(4 downto 0) := (others => '0');
signal  Data_8 : std_logic_vector(4 downto 0) := (others => '0');
signal  Data_9 : std_logic_vector(4 downto 0) := (others => '0');

--signal  coreOUT : std_logic_vector(15 downto 0) := (others => '0');

begin

circ_buf: BUF 
  port map(
         clk => clk,
        w_en => w_en,
      inDATA => inDATA,
      Data_1 => Data_1,
      Data_2 => Data_2,
      Data_3 => Data_3,
      Data_4 => Data_4,
      Data_5 => Data_5,
      Data_6 => Data_6,
      Data_7 => Data_7,
      Data_8 => Data_8,
      Data_9 => Data_9
);


circ_cnv: CONV 
  port map(
      clk    => clk ,
      en     => cnv_en ,
      shift  => shift ,
      ch_cernel => ch_cernel,
      info_1 => Data_1 ,
      info_2 => Data_2 ,
      info_3 => Data_3 ,
      info_4 => Data_4 ,
      info_5 => Data_5 ,
      info_6 => Data_6 ,
      info_7 => Data_7 ,
      info_8 => Data_8 ,
      info_9 => Data_9 ,
      cnvOUT => coreOUT 
);

end CORE_strucT ; -- CONV_strucT