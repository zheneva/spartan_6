----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 12.04.2023 13:21:51
-- Design Name: 
-- Module Name: top_level - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity top_level is
    Port (clk_50MHz : in STD_LOGIC;
           clr : in STD_LOGIC;
     ch_cernel : in std_logic;     
           vga_hs   : out std_logic;
           vga_vs   : out std_logic;
           vga_r : out std_logic_vector(4 downto 0);
           vga_g : out std_logic_vector(5 downto 0);
           vga_b : out std_logic_vector(4 downto 0);
			  no_bip: out std_logic
           );
end top_level;

architecture Behavioral of top_level is

component core_gen 
  generic(
    --mode : boolean := true;
    H_pixel   : integer :=  640; -- //? 640; 
    V_lines   : integer :=  480;-- //? 480;
    H_f_porch : integer :=  16;  -- //? 16 ; 
    H_b_porch : integer :=  48;  -- //? 16 ; 
    H_s_pulse : integer :=  96;  -- //? 128;
    V_f_porch : integer :=  10; -- //? 10 ; 
    V_b_porch : integer :=  29; -- //? 29 ;
    V_s_pulse : integer :=  2  -- //? 2
    );
  port (
    clk : in std_logic;
    clr : in std_logic;
  ch_cernel : in std_logic;
    vga_hs   : out std_logic;
    vga_vs   : out std_logic;
    vga_r : out std_logic_vector(4 downto 0);
    vga_g : out std_logic_vector(5 downto 0);
    vga_b : out std_logic_vector(4 downto 0)
  );
end component;

component clk_wiz
port
 (-- Clock in ports
  clk_50MHz           : in     std_logic;
  -- Clock out ports
  clk_25MHz          : out    std_logic
 );
end component;

signal clk_25MHz : std_logic;

begin


circ_clk : clk_wiz
  port map
   (-- Clock in ports
    clk_50MHz => clk_50MHz,
    -- Clock out ports
    clk_25MHz => clk_25MHz);

circ_core:  core_gen 
 port map(
    clk => clk_25MHz,
    clr => clr,
ch_cernel => ch_cernel,
    vga_hs => vga_hs,
    vga_vs =>  vga_vs,
    vga_r =>  vga_r,
    vga_g => vga_g,
    vga_b => vga_b
);

no_bip <= '1';

end Behavioral;
