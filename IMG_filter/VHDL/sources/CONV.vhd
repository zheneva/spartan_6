library IEEE;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity CONV is
    port (
        clk    : in std_logic;
        en     : in std_logic;
        shift  : in natural;
        ch_cernel : in std_logic;
    
        info_1 : in std_logic_vector(4 downto 0);
        info_2 : in std_logic_vector(4 downto 0);
        info_3 : in std_logic_vector(4 downto 0);
        info_4 : in std_logic_vector(4 downto 0);
        info_5 : in std_logic_vector(4 downto 0);
        info_6 : in std_logic_vector(4 downto 0);
        info_7 : in std_logic_vector(4 downto 0);
        info_8 : in std_logic_vector(4 downto 0);
        info_9 : in std_logic_vector(4 downto 0);
        cnvOUT : out std_logic_vector(4 downto 0)
    );
end CONV;

architecture CONV_strucr of CONV is

component  sum is
    port (
        Data_1 : in std_logic_vector(9 downto 0);
        Data_2 : in std_logic_vector(9 downto 0);
        Data_3 : in std_logic_vector(9 downto 0);
        Data_4 : in std_logic_vector(9 downto 0);
        Data_5 : in std_logic_vector(9 downto 0);
        Data_6 : in std_logic_vector(9 downto 0);
        Data_7 : in std_logic_vector(9 downto 0);
        Data_8 : in std_logic_vector(9 downto 0);
        Data_9 : in std_logic_vector(9 downto 0);
        sum_out : out std_logic_vector(9 downto 0)
    );
end component;

component MULT 
    generic(
    N : integer := 5
    );
    port ( 
        pixel    : in std_logic_vector (N-1 downto 0);
        factor   : in std_logic_vector (N-1 downto 0);
        mult_out : out std_logic_vector (2*N-1 downto 0)
    );
end component;

component REGsh 
    generic(
      N : integer := 10        -- 16
    );
    port (
      clk : in std_logic;
      en  : in std_logic;
      shift   : natural;
      inaData : in std_logic_vector(N-1 downto 0);
      outData : out std_logic_vector((N-1)/2 downto 0)
    ) ;
end component;

signal koef_1 : std_logic_vector(4 downto 0) ; --"00001" ;
signal koef_2 : std_logic_vector(4 downto 0) ; --"00010" ;
signal koef_3 : std_logic_vector(4 downto 0) ; --"00001" ;
signal koef_4 : std_logic_vector(4 downto 0) ;--"00010" ;
signal koef_5 : std_logic_vector(4 downto 0) ; --"00100" ;
signal koef_6 : std_logic_vector(4 downto 0) ; --"00010" ;
signal koef_7 : std_logic_vector(4 downto 0) ;
signal koef_8 : std_logic_vector(4 downto 0) ; --"00010" ;
signal koef_9 : std_logic_vector(4 downto 0)  ;

signal mult_out_1 : std_logic_vector(9 downto 0) := (others => '0') ;
signal mult_out_2 : std_logic_vector(9 downto 0) := (others => '0') ;
signal mult_out_3 : std_logic_vector(9 downto 0) := (others => '0') ;
signal mult_out_4 : std_logic_vector(9 downto 0) := (others => '0') ;
signal mult_out_5 : std_logic_vector(9 downto 0) := (others => '0') ;
signal mult_out_6 : std_logic_vector(9 downto 0) := (others => '0') ;
signal mult_out_7 : std_logic_vector(9 downto 0) := (others => '0') ;
signal mult_out_8 : std_logic_vector(9 downto 0) := (others => '0') ;
signal mult_out_9 : std_logic_vector(9 downto 0) := (others => '0') ;

signal sum_out    : std_logic_vector(9 downto 0) := (others => '0') ;




begin

process(ch_cernel )
 begin
    if (ch_cernel = '1') then 
        koef_1 <= "00000" ; --"00001" ;
        koef_2 <= "00000"; --"00010" ;
        koef_3 <= "00000" ; --"00001" ;
        koef_4 <= "00000" ;--"00010" ;
        koef_5 <= "00001" ; --"00100" ;
        koef_6 <= "00000" ; --"00010" ;
        koef_7 <= "00000" ;
        koef_8 <= "00000" ; --"00010" ;
        koef_9 <= "00000" ;
     else 
        koef_1 <= "00001" ; --"00001" ;
        koef_2 <= "00010" ; --"00010" ;
        koef_3 <= "00001" ; --"00001" ;
        koef_4 <= "00010" ;--"00010" ;
        koef_5 <= "00100" ; --"00100" ;
        koef_6 <= "00010" ; --"00010" ;
        koef_7 <= "00001" ;
        koef_8 <= "00010" ; --"00010" ;
        koef_9 <= "00001" ;    
     end if;
end process;


circ_mult_1: MULT 
    port map( 
        pixel    => info_1,    
        factor   => koef_1,    
        mult_out => mult_out_1 
);
circ_mult_2: MULT 
    port map( 
        pixel    => info_2,    
        factor   => koef_2,    
        mult_out => mult_out_2 
);
circ_mult_3: MULT 
    port map( 
        pixel    => info_3,    
        factor   => koef_3,    
        mult_out => mult_out_3 
);
circ_mult_4: MULT 
    port map( 
        pixel    => info_4,    
        factor   => koef_4,    
        mult_out => mult_out_4 
);
circ_mult_5: MULT 
    port map( 
        pixel    => info_5,    
        factor   => koef_5,    
        mult_out => mult_out_5 
);
circ_mult_6: MULT 
    port map( 
        pixel    => info_6,    
        factor   => koef_6,    
        mult_out => mult_out_6 
);
circ_mult_7: MULT 
    port map( 
        pixel    => info_7,    
        factor   => koef_7,    
        mult_out => mult_out_7 
);
circ_mult_8: MULT 
    port map( 
        pixel    => info_8,    
        factor   => koef_8,    
        mult_out => mult_out_8 
);
circ_mult_9: MULT 
    port map( 
        pixel    => info_8,    
        factor   => koef_9,    
        mult_out => mult_out_9 
);

circ_sum:  sum 
    port map(
        Data_1 => mult_out_1,
        Data_2 => mult_out_2,
        Data_3 => mult_out_3,
        Data_4 => mult_out_4,
        Data_5 => mult_out_5,
        Data_6 => mult_out_6,
        Data_7 => mult_out_7,
        Data_8 => mult_out_8,
        Data_9 => mult_out_9,
        sum_out => sum_out
);

circ_regsh :  REGsh 
  port map(
      clk     => clk,
      en      => en,
      shift   => shift,
      inaData => sum_out,
      outData => cnvOUT
) ;

end architecture;