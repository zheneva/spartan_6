library IEEE;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity REGsh is
  generic(
    N : integer         -- 16
  );
  port (
    clk : in std_logic;
    en  : in std_logic;
    shift   : natural;
    inaData : in std_logic_vector(N-1 downto 0);
    outData : out std_logic_vector((N-1)/2 downto 0)
  ) ;
end REGsh;

architecture REGsh_struct of REGsh is

  signal inData_tmp : std_logic_vector(N-1 downto 0);

begin

  process(clk)
  begin
    if (en = '1') then
      inData_tmp <= std_logic_vector(shift_right(signed(inaData), shift));
    else 
      -- inData_tmp <= inData_tmp;
      inData_tmp <= (others => '0');
    end if;
  end process;
  outData <= inData_tmp(4 downto 0);
end REGsh_struct ; -- REGsh_struct