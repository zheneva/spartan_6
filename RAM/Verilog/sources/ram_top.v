

module ram_top (
    input clk_50MHz,
    input [3:0] DINA,
    input [2:0] ADDR,
    input WEA,
    output [3:0] o_RAM_out,
    output reg no_bip 
);
    
    

// reg [2:0]   ADDR_cnt  ;// = 4'b0111;
// reg [2:0] 	D_ADDR_cnt;// = 4'b0111;
// reg [7:0]   D_DINA_cnt;// = 8'b0000_0000;
// wire[7:0]     DINA_cnt;
// reg [2:0]   Byte_cnt  ;//= 3'b000;
// reg WEA;
// reg D_WEA;


// reg en_Byte_cnt;//    = 1'b0;

// always @(negedge clk_50MHz) begin
//   if (rst == 1) 
//     ADDR_cnt <= -1;//4'b0111;
//   else begin
//     ADDR_cnt <= ADDR_cnt + 4'b0001;
//   end
// end

// always @(negedge clk_50MHz) begin
//   if (ADDR_cnt == 3'b110) 
//     en_Byte_cnt <= 1'b1;
//   else 
//     en_Byte_cnt <= 1'b0;  
// end

// always @(negedge clk_50MHz) begin
//   if (rst == 1) 
//     Byte_cnt[2:0] <= 3'b000;
//   else begin
//     if (en_Byte_cnt == 1) 
//         Byte_cnt[2:0] <= Byte_cnt[2:0] + 3'b001;
//   end
// end

// always @(negedge clk_50MHz) begin
//   DINA_cnt[7:0] <= {1'b0, Byte_cnt, 1'b0, ADDR_cnt};
//   //DINA_cnt[3:0] <= ADDR_cnt[3:0];
//   //DINA_cnt[7:4] <= Byte_cnt[3:0];
//   //D_DINA_cnt <= DINA_cnt;
// end


// always @(negedge clk_50MHz) begin
//   D_ADDR_cnt[2:0] <= ADDR_cnt[2:0];
// end


// always @(negedge clk_50MHz) begin
//   if (rst == 1) 
//     WEA <= 1'b0;
//   else
//     WEA <= 1'b1;

//     D_WEA <= WEA;
// end


always @(posedge clk_50MHz) begin
  no_bip <= 1'b1;
end


single_port_ram your_instance_name (
  .clka(clk_50MHz), // input clka
  .wea(~WEA), // input [0 : 0] wea
  .addra(ADDR[2:0]), // input [3 : 0] addra
  .dina(DINA[3:0]), // input [7 : 0] dina
  .douta(o_RAM_out[3:0]) // output [7 : 0] douta
);


endmodule