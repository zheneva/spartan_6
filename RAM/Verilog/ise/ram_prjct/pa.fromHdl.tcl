
# PlanAhead Launch Script for Pre-Synthesis Floorplanning, created by Project Navigator

create_project -name ram_prjct -dir "D:/Androsov/spartan_6/RAM/Verilog/ise/ram_prjct/planAhead_run_3" -part xc6slx9tqg144-3
set_param project.pinAheadLayout yes
set srcset [get_property srcset [current_run -impl]]
set_property target_constrs_file "ram_top.ucf" [current_fileset -constrset]
add_files [list {ipcore_dir/single_port_ram.ngc}]
set hdlfile [add_files [list {ipcore_dir/single_port_ram.v}]]
set_property file_type Verilog $hdlfile
set_property library work $hdlfile
set hdlfile [add_files [list {../../sources/ram_top.v}]]
set_property file_type Verilog $hdlfile
set_property library work $hdlfile
set_property top ram_top $srcset
add_files [list {ram_top.ucf}] -fileset [get_property constrset [current_run]]
add_files [list {ipcore_dir/single_port_ram.ncf}] -fileset [get_property constrset [current_run]]
open_rtl_design -part xc6slx9tqg144-3
