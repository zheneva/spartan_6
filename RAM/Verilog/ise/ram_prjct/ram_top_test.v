

module rem_top_test (
);




reg clk_50MHz = 0;
reg [7:0] DINA = 0;
reg [3:0] ADDR = 0;
reg WEA;
wire [7:0] o_RAM_out;

integer n; 
    
ram_top MRAM(
      .clk_50MHz(clk_50MHz)
    , .DINA(DINA)
    , .ADDR(ADDR)
    , .WEA(WEA)
    , .o_RAM_out(o_RAM_out) 
);





always begin
    #20; clk_50MHz <= ~clk_50MHz;
end


initial begin
    n = 0;
    WEA <= 0;
    #10;
			
		  WEA <= 1;
        DINA <= (2**n)-1; n = n+1; ADDR <= 0; #40;
        DINA <= (2**n)-1; n = n+1; ADDR <= ADDR + 1; #40;
        DINA <= (2**n)-1; n = n+1; ADDR <= ADDR + 1; #40;
		  DINA <= (2**n)-1; n = n+1; ADDR <= ADDR + 1; #40;
		  DINA <= (2**n)-1; n = n+1; ADDR <= ADDR + 1; #40;
		  DINA <= (2**n)-1; n = n+1; ADDR <= ADDR + 1; #40;
		  DINA <= (2**n)-1; n = n+1; ADDR <= ADDR + 1; #40;
		  DINA <= (2**n)-1; n = n+1; ADDR <= 0; #40;
		  WEA <= 0;
		  DINA <= (2**n)-1; n = n+1; ADDR <= ADDR + 1; #40;
        DINA <= (2**n)-1; n = n+1; ADDR <= ADDR + 1; #40;
        DINA <= (2**n)-1; n = n+1; ADDR <= ADDR + 1; #40;
		  DINA <= (2**n)-1; n = n+1; ADDR <= ADDR + 1; #40;
		  DINA <= (2**n)-1; n = n+1; ADDR <= ADDR + 1; #40;
		  DINA <= (2**n)-1; n = n+1; ADDR <= ADDR + 1; #40;
		  DINA <= (2**n)-1; n = n+1; ADDR <= ADDR + 1; #40;
		  DINA <= (2**n)-1; n = n+1; ADDR <= ADDR + 1; #40;

end


endmodule