`timescale 1ns / 1ps

////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer:
//
// Create Date:   13:58:28 07/27/2023
// Design Name:   rem_top_test
// Module Name:   D:/Androsov/spartan_6/RAM/Verilog/ise/ram_prjct/test.v
// Project Name:  ram_prjct
// Target Device:  
// Tool versions:  
// Description: 
//
// Verilog Test Fixture created by ISE for module: rem_top_test
//
// Dependencies:
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
////////////////////////////////////////////////////////////////////////////////

module test;

	// Outputs
	wire ;

	// Instantiate the Unit Under Test (UUT)
	rem_top_test uut (
		.()
	);

	initial begin
		// Initialize Inputs

		// Wait 100 ns for global reset to finish
		#100;
        
		// Add stimulus here

	end
      
endmodule

