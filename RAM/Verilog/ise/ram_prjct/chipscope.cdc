#ChipScope Core Inserter Project File Version 3.0
#Fri Jul 28 07:27:56 VET 2023
Project.device.designInputFile=D\:\\Androsov\\spartan_6\\RAM\\Verilog\\ise\\ram_prjct\\ram_top_cs.ngc
Project.device.designOutputFile=D\:\\Androsov\\spartan_6\\RAM\\Verilog\\ise\\ram_prjct\\ram_top_cs.ngc
Project.device.deviceFamily=18
Project.device.enableRPMs=true
Project.device.outputDirectory=D\:\\Androsov\\spartan_6\\RAM\\Verilog\\ise\\ram_prjct\\_ngo
Project.device.useSRL16=true
Project.filter.dimension=1
Project.filter<0>=
Project.icon.boundaryScanChain=1
Project.icon.enableExtTriggerIn=false
Project.icon.enableExtTriggerOut=false
Project.icon.triggerInPinName=
Project.icon.triggerOutPinName=
Project.unit.dimension=1
Project.unit<0>.clockChannel=clk_50MHz_BUFGP
Project.unit<0>.clockEdge=Rising
Project.unit<0>.dataChannel<0>=D_WEA
Project.unit<0>.dataChannel<10>=DINA_cnt<7>
Project.unit<0>.dataChannel<11>=o_RAM_out_0_OBUF
Project.unit<0>.dataChannel<12>=o_RAM_out_1_OBUF
Project.unit<0>.dataChannel<13>=o_RAM_out_2_OBUF
Project.unit<0>.dataChannel<14>=o_RAM_out_3_OBUF
Project.unit<0>.dataChannel<15>=o_RAM_out_4_OBUF
Project.unit<0>.dataChannel<16>=o_RAM_out_5_OBUF
Project.unit<0>.dataChannel<17>=o_RAM_out_6_OBUF
Project.unit<0>.dataChannel<18>=o_RAM_out_7_OBUF
Project.unit<0>.dataChannel<1>=D_ADDR_cnt<0>
Project.unit<0>.dataChannel<2>=D_ADDR_cnt<1>
Project.unit<0>.dataChannel<3>=D_ADDR_cnt<2>
Project.unit<0>.dataChannel<4>=DINA_cnt<0>
Project.unit<0>.dataChannel<5>=DINA_cnt<1>
Project.unit<0>.dataChannel<6>=DINA_cnt<2>
Project.unit<0>.dataChannel<7>=DINA_cnt<4>
Project.unit<0>.dataChannel<8>=DINA_cnt<5>
Project.unit<0>.dataChannel<9>=DINA_cnt<6>
Project.unit<0>.dataDepth=4096
Project.unit<0>.dataEqualsTrigger=true
Project.unit<0>.dataPortWidth=12
Project.unit<0>.enableGaps=false
Project.unit<0>.enableStorageQualification=true
Project.unit<0>.enableTimestamps=false
Project.unit<0>.timestampDepth=0
Project.unit<0>.timestampWidth=0
Project.unit<0>.triggerChannel<0><0>=WEA_IBUF
Project.unit<0>.triggerChannel<1><0>=ADDR_0_IBUF
Project.unit<0>.triggerChannel<1><1>=ADDR_1_IBUF
Project.unit<0>.triggerChannel<1><2>=ADDR_2_IBUF
Project.unit<0>.triggerChannel<2><0>=DINA_0_IBUF
Project.unit<0>.triggerChannel<2><1>=DINA_1_IBUF
Project.unit<0>.triggerChannel<2><2>=DINA_2_IBUF
Project.unit<0>.triggerChannel<2><3>=DINA_3_IBUF
Project.unit<0>.triggerChannel<2><4>=DINA_cnt<5>
Project.unit<0>.triggerChannel<2><5>=DINA_cnt<6>
Project.unit<0>.triggerChannel<2><6>=DINA_cnt<7>
Project.unit<0>.triggerChannel<3><0>=o_RAM_out_0_OBUF
Project.unit<0>.triggerChannel<3><1>=o_RAM_out_1_OBUF
Project.unit<0>.triggerChannel<3><2>=o_RAM_out_2_OBUF
Project.unit<0>.triggerChannel<3><3>=o_RAM_out_3_OBUF
Project.unit<0>.triggerChannel<3><4>=o_RAM_out_4_OBUF
Project.unit<0>.triggerChannel<3><5>=o_RAM_out_5_OBUF
Project.unit<0>.triggerChannel<3><6>=o_RAM_out_6_OBUF
Project.unit<0>.triggerChannel<3><7>=o_RAM_out_7_OBUF
Project.unit<0>.triggerConditionCountWidth=0
Project.unit<0>.triggerMatchCount<0>=1
Project.unit<0>.triggerMatchCount<1>=1
Project.unit<0>.triggerMatchCount<2>=1
Project.unit<0>.triggerMatchCount<3>=1
Project.unit<0>.triggerMatchCountWidth<0><0>=0
Project.unit<0>.triggerMatchCountWidth<1><0>=0
Project.unit<0>.triggerMatchCountWidth<2><0>=0
Project.unit<0>.triggerMatchCountWidth<3><0>=0
Project.unit<0>.triggerMatchType<0><0>=1
Project.unit<0>.triggerMatchType<1><0>=1
Project.unit<0>.triggerMatchType<2><0>=1
Project.unit<0>.triggerMatchType<3><0>=1
Project.unit<0>.triggerPortCount=4
Project.unit<0>.triggerPortIsData<0>=true
Project.unit<0>.triggerPortIsData<1>=true
Project.unit<0>.triggerPortIsData<2>=true
Project.unit<0>.triggerPortIsData<3>=true
Project.unit<0>.triggerPortWidth<0>=1
Project.unit<0>.triggerPortWidth<1>=3
Project.unit<0>.triggerPortWidth<2>=4
Project.unit<0>.triggerPortWidth<3>=4
Project.unit<0>.triggerSequencerLevels=16
Project.unit<0>.triggerSequencerType=1
Project.unit<0>.type=ilapro
