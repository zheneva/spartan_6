library IEEE;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity led_test is
end led_test;

architecture led_test_sruct of led_test is

component led_control
    port (
        clk    : in std_logic;
        mode   : in std_logic_vector(2 downto 0);

        led_o  : out std_logic_vector(11 downto 0);
        
        no_bip : out std_logic 
    ) ;
end component;

    signal clk       : std_logic := '0'; 
    signal no_bip    : std_logic; 
    signal mode      : std_logic_vector(2 downto 0); 
 
    signal led_o  : std_logic_vector(11 downto 0); 

begin

mode   <= "100" after 0 ns,
          "010" after 4000 ms,
          "001" after 8000 ms;


process
 begin
    wait for 5 ns;
    clk <= not clk;
end process ; 

circ: led_control
        port map(
            clk    => clk,
            mode   => mode,
            no_bip => no_bip,
            led_o  => led_o 
        ) ;

 end architecture;