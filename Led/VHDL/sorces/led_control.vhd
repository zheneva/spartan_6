library IEEE;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity led_control is
  port (
    clk    : in std_logic;
    mode   : in std_logic_vector(2 downto 0);

    led_o  : out std_logic_vector(11 downto 0);
    
    no_bip : out std_logic 
  ) ;
end led_control;

architecture led_control_behaviral of led_control is
    

    signal leds    : std_logic_vector(11 downto 0) := (others => '0') ;

    --signal mode    : std_logic_vector(2 downto 0) ;

    signal CLK_DIV : std_logic_vector(25 downto 0) ;
    signal cnt     : std_logic_vector(25 downto 0) := (others => '0') ;

begin


led : process(clk)
 begin 

    if rising_edge(clk) then
        if cnt = CLK_DIV then
            cnt <= (others => '0');
            leds <= not leds;
        else
            cnt <= std_logic_vector(unsigned(cnt) + 1);
        end if ;
    end if;
end process;


mod_switch : process( mode )
 begin
    case( mode ) is
    
        when "001" =>
                CLK_DIV <=(others => '1') ;
                --?leds <=  (0 => '0', 1 => '0', 2 => '0', others => '1');

        when "010" =>
                CLK_DIV <= (25 => '1', others => '0') ;
                --?leds <=  (3 => '0', 4 => '1', 5 => '0', others => '1');

        when "100" =>
                CLK_DIV <= (24 => '1', others => '0') ;
                --?leds <=  (6 => '0', 7 => '0', 8 => '0', others => '1');
    
        when others =>
                CLK_DIV <= (others => '0')  ;
                --?leds <=  (9 => '0', 10 => '0', 11 => '0', others => '1');
    end case ;
end process ; -- mode


led_o <= leds;

no_bip <= '1';





end led_control_behaviral ; -- led_control_behaviral