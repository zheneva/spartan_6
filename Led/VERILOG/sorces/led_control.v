
module led_control (clk, mode, no_bip, leds_o);

    input clk;
    input [2:0] mode;
    
    output reg no_bip;
    output reg [11:0] leds_o;

    reg [11:0] leds;
    reg [25:0] CLK_DIV = 0;
    reg [25:0] cnt = 0;


    always @(posedge clk) begin
        
        case (mode)
            3'b001 : CLK_DIV = 16777215; 
            3'b010 : CLK_DIV = 8388608; 
            3'b100 : CLK_DIV = 4194304; 
            default: CLK_DIV = 0 ;
        endcase
    end


    always @(posedge clk) begin
        if (cnt == CLK_DIV ) begin
            cnt <= 'd0;
            leds = ~ leds;
        end
        else begin 
            cnt <= cnt + 1'd1;
        end
    end

    always @(posedge clk) begin
        leds_o = leds;
        no_bip = 1'b1;
    end

endmodule